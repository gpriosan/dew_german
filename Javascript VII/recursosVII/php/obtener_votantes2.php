   <?php
   		$responsable = isset($_GET['responsable'])? $_GET['responsable']:"";
   		$votantes = isset($_GET['votantes'])? intval($_GET['votantes']):0;
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=usuarios1', 'usuarioWeb', 'claveWeb');
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->exec("SET NAMES 'utf8'");	

			$sql_cons = "SELECT a.id as id, a.junta_electoral as junta_electoral, a.responsable as responsable, c.descripcion as cargo, a.email as email, a.num_votantes as num_votan, a.num_blanco as num_blanco";
			$sql_cons = $sql_cons." FROM andalucia as a  LEFT JOIN cargo as c on a.cargo=c.id ";
			$sql_cons = $sql_cons."WHERE a.responsable LIKE '%".$responsable."%' AND a.num_votantes >= ".$votantes. " ORDER BY a.id";
			$consulta= $pdo->prepare($sql_cons);
			
			$consulta->execute();
			$resultado=$consulta->fetchAll(PDO::FETCH_OBJ);	

			echo "<table><thead><tr><th>Id</th><th>Junta Provincial</th><th>Responsable</th><th>Cargo</th><th>Email</th><th>Número de votantes</th><th>Votos en blanco</th></tr></thead><tbody>";
			//echo var_dump($resultado);
			foreach ($resultado as $r) {
				echo "<tr><td>".$r->id."</td><td>".$r->junta_electoral."</td><td>".$r->responsable."</td><td>".$r->cargo."</td><td>".$r->email."</td><td>".$r->num_votan."</td><td>".$r->num_blanco;
			}
			echo "</tbody></table>";						   
			$pdo=null;	// cerramos la conexion					
		} catch(Exception $e){
			die($e->getMessage());
		}

	?>  


