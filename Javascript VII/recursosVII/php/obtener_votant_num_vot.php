   <?php
		
   		$min = isset($_POST['min'])? $_POST['min']:"0";
   		$max = isset($_POST['max'])? $_POST['max']:"99999999999";
		try {
			$pdo = new PDO('mysql:host=localhost;dbname=usuarios1', 'usuarioWeb', 'claveWeb');
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo->exec("SET NAMES 'utf8'");	

			$sql_cons = "SELECT a.id as id, a.junta_electoral as junta_electoral, a.responsable as responsable, c.descripcion as cargo, a.email as email, a.num_votantes as num_votan, a.num_blanco as num_blanco";
			$sql_cons .= " FROM andalucia as a  LEFT JOIN cargo as c on a.cargo=c.id ";
			$sql_cons .= "WHERE a.num_votantes >".$min." AND a.num_votantes <=".$max. " ORDER BY a.id";
			$consulta= $pdo->prepare($sql_cons);
			$consulta->execute();
			$resultado=$consulta->fetchAll(PDO::FETCH_OBJ);	

			$datos = array();
			foreach ($resultado as $r) {
				$datos[] = $r;
			}
			echo json_encode( $datos);					   
			$pdo=null;	// cerramos la conexion					
		} catch(Exception $e){
			die($e->getMessage());
		}

	?>  



