//Variable global que contendrá el objeto ajax
var objetoAjax;
var objetoAjax2;
var objetoAjax4;

//Iniciamos la página


function agregaEvento(evento, objeto, funcion) {
	if (objeto.addEventListener) // estamos en un DOM del estándar W3C
		objeto.addEventListener(evento,funcion,false);
	else if (objeto.attachEvent) // estamos en un DOM de IExplorer
		objeto.attachEvent("on"+evento, funcion);
	else // no estamos en un DOM ni W3C ni de IExplorer ? modelo tradicional
	objeto["on"+evento] = funcion; // recuerda: objetos son arrays asociativos
}
agregaEvento("load", window, iniciar);

function crearAjax() {
	var objeto = null; // será el objeto a crear
	try {
		objeto = new XMLHttpRequest(); // navegadores estándar
	} catch (error1) {
		try {
			objeto = new ActiveXObject("Microsoft.XMLHTTP"); // IE con librería MSXML 2.0 o superior
		} catch (error2) {
			try {
				objeto = new ActiveXObject("Msxml12.XMLHTTP"); // IE con librería MSXML 1.2
			} catch (error3) {
				objeto = false; // no hemos logrado crear el objeto, retornamos false
			}
		}
	}
	return objeto; // devolvemos el objeto
}



function iniciar () {
	var obtener = document.getElementById("obtener");
	agregaEvento("click", obtener, filtrando);

	objetoAjax = crearAjax();
	objetoAjax2 = crearAjax();
	objetoAjax4 = crearAjax();
	if (!objetoAjax) {
		alert("No se ha podido crear el objeto ajax relacionado con las islas");
		return;
	}
	//busco las islas utilizando ajax con GET
	objetoAjax.onreadystatechange = colocar_islas;
	objetoAjax.open("GET", "php/obtener_islas.php", true);
	objetoAjax.send(null);

	
}
function colocar_islas() {
	var select_islas = document.getElementById("islas");
	if (this.readyState == 4) { // la petición terminó
		if (this.status == 200) { // la petición terminó con éxito
			select_islas.innerHTML = objetoAjax.responseText;
		} 
		else {
			alert("Petición terminada con error: " + this.status + " - " + this.statusText);
		}
	} else { // la petición no ha terminado
	}
	//var islas = document.getElementById("islas");
	agregaEvento("change",select_islas,solicitar_municipios);
	solicitar_municipios();
	
}
function solicitar_municipios(){
	var isla = document.getElementById("islas").value;
	var url = "php/obtener_municipios.php?isla="+encodeURIComponent(isla);
	objetoAjax2.onreadystatechange = colocar_municipios;
	objetoAjax2.open("GET",url, true);
	objetoAjax2.send(null);
	
}
function colocar_municipios() {
	var select_m = document.getElementById("municipios");
	var municipiosLoading = document.getElementById("municipiosLoading");
	municipiosLoading.innerHTML="<img src='imagenes/ajax_clock_small.gif'></a> Cargando";	
	if (this.readyState == 4) { // la petición terminó
		if (this.status == 200) { // la petición terminó con éxito
			select_m.innerHTML = objetoAjax2.responseText;
			municipiosLoading.innerHTML="";
		} 
		else {
			alert("Petición terminada con error: " + this.status + " - " + this.statusText);
		}
	} else { // la petición no ha terminado
	}
}
//Ejercicio 2-4
function filtrando(){
	var responsablesT = document.getElementById("responsableText").value;
	var votantesT = document.getElementById("votanteText").value;
	objetoAjax4.onreadystatechange = filtrar;
	//url="php/obtener_votantes2.php?responsable="+encodeURIComponent(responsablesT)+"&votantes="+encodeURIComponent(votantesT);
	//objetoAjax4.open("GET", url, true);
	objetoAjax4.open("POST", "php/obtener_votantes_post.php", true);
	try{
		objetoAjax4.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="responsable="+encodeURIComponent(responsablesT)+"&votantes="+encodeURIComponent(votantesT);
		objetoAjax4.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function filtrar() {
	var resultado = document.getElementById("resultadosVotantes");
	/**if (this.readyState == 4) { // la petición terminó
		if (this.status == 200) { // la petición terminó con éxito
			resultado.innerHTML = objetoAjax4.responseText;
			doscol();
		} 
		else {
			alert("Petición terminada con error: " + this.status + " - " + this.statusText);
		}
	} else { // la petición no ha terminado
	}**/

	//ejercicio 3-1

	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				resultado.innerHTML = objetoAjax4.responseText;
				resultado.innerHTML += "Estado 200: "+ this.statusText;
				break;
			case 404: // no se encontró el recurso
				resultado.innerHTML = "No es posible recuperar los resultados filtrados <br> Error 404 :"+this.statusText;
				break;
			default:
				resultado.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
	
}
//Ejercicio 2-5
function doscol(){
	var div = document.getElementById("resultadosVotantes");
	var headtabla = document.getElementsByTagName('tr');
	
	var tabla = document.getElementsByTagName('tbody');
	for (var i = 0; i < headtabla.length; i++) {
		if (i==0){
			th=document.createElement("th");
			th2=document.createElement("th");
			txtE=document.createTextNode("Editar");
			th.appendChild(txtE);
			headtabla[i].appendChild(th);

			txtEl=document.createTextNode("Eliminar");
			th2.appendChild(txtEl);
			headtabla[i].appendChild(th2);
		}else{
			td=document.createElement("td");
			img=document.createElement("img");
			img.src="imagenes/editar.png";
			td.appendChild(img);
			headtabla[i].appendChild(td);

			td2=document.createElement("td");
			img=document.createElement("img");
			img.src="imagenes/eliminar.png";
			td2.appendChild(img);
			headtabla[i].appendChild(td2);
		}
	}
	

}
