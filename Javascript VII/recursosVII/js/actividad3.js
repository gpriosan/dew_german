var objetoAjax;
var objetoAjax2;
var objetoAjax3;
var objetoAjax2_1;
var objetoAjax1_2;
var objetoAjaxRellena;
var objetoAjaxUpdate;

///////////////////////INICIO ACT 4 //////////////7

function agregaEvento(evento, objeto, funcion) {
	if (objeto.addEventListener) // estamos en un DOM del estándar W3C
		objeto.addEventListener(evento,funcion,false);
	else if (objeto.attachEvent) // estamos en un DOM de IExplorer
		objeto.attachEvent("on"+evento, funcion);
	else // no estamos en un DOM ni W3C ni de IExplorer ? modelo tradicional
	objeto["on"+evento] = funcion; // recuerda: objetos son arrays asociativos
}
agregaEvento("load", window, iniciar);

function crearAjax() {
	var objeto = null; // será el objeto a crear
	try {
		objeto = new XMLHttpRequest(); // navegadores estándar
	} catch (error1) {
		try {
			objeto = new ActiveXObject("Microsoft.XMLHTTP"); // IE con librería MSXML 2.0 o superior
		} catch (error2) {
			try {
				objeto = new ActiveXObject("Msxml12.XMLHTTP"); // IE con librería MSXML 1.2
			} catch (error3) {
				objeto = false; // no hemos logrado crear el objeto, retornamos false
			}
		}
	}
	return objeto; // devolvemos el objeto
}
function limpiar(){
	var poblacion = document.getElementById("poblacion").innerHTML="";
	var sugerencias = document.getElementById("sugerencias").innerHTML="";
	var municipio = document.getElementById("municipio").value="";
}

function iniciar () {
	var grabar=document.getElementById("update");
	agregaEvento("click", grabar, grabarCambios);
	objetoAjax = crearAjax();
	objetoAjax2 = crearAjax();
	objetoAjax3 = crearAjax();
	objetoAjax1_2 = crearAjax();
	objetoAjax2_1 = crearAjax();
	if (!objetoAjax) {
		alert("No se ha podido crear el objeto ajax relacionado con los cargos");
		return;
	}
	else if (!objetoAjax2) {
		alert("No se ha podido crear el objeto ajax relacionado con los votantes");
		return;
	}
	
	objetoAjax.onreadystatechange = rellenarCargos;
	objetoAjax2.onreadystatechange = rellenarVotantes;

	objetoAjax3.onreadystatechange = resultadosEmail;
	objetoAjax1_2.onreadystatechange = resultadosCargos;
	objetoAjax2_1.onreadystatechange = resultadosVotantes;

	objetoAjax.open("POST", "php/obtener_cargos.php", true);
	objetoAjax2.open("POST", "php/obtener_rangos.php", true);
	objetoAjax3.open("POST", "php/obtener_votant_email.php", true);
	
	
	
	try{
		var cargo = document.getElementById("cargo");
		objetoAjax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		objetoAjax.send("");
		objetoAjax2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		objetoAjax2.send("");

		var email = document.getElementById("email");
		var datos="email="+encodeURIComponent(email.value);
		objetoAjax3.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		objetoAjax3.send(datos);

		
		
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
	try{
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function actCargo(){
	var cargo = document.getElementById("cargo");
	objetoAjax1_2.open("POST", "php/obtener_votant_cargo.php", true);
	var datos2="id_cargo="+encodeURIComponent(cargo.value);
	objetoAjax1_2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	objetoAjax1_2.send(datos2);
}
function actVotantes(){
	var votantes = document.getElementById("votantes");
	if (votantes.value==1){
		var min=0;
		var max=500000;
	}		
	if (votantes.value==2){
		var min=500000;
		var max=1000000;
	}
	if (votantes.value==3){
		var min=1000000;
		var max=1500000;
	}
	if (votantes.value==4){
		var min=1;
		var max=2147483647;
	}
	objetoAjax2_1.open("POST", "php/obtener_votant_num_vot.php", true);
	var datos3="min='"+encodeURIComponent(min)+"'&max='"+encodeURIComponent(max)+"'";
	objetoAjax2_1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	objetoAjax2_1.send(datos3);
}
function actEmail(){
	objetoAjax3.open("POST", "php/obtener_votant_email.php", true);
	var email = document.getElementById("email");
	var datos="email="+encodeURIComponent(email.value);
	objetoAjax3.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	objetoAjax3.send(datos);
}
function rellenarCargos(){
	var cargo = document.getElementById("cargo");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				cargo.innerHTML = objetoAjax.responseText;
				objetoAjax1_2.open("POST", "php/obtener_votant_cargo.php", true);
				var datos2="id_cargo="+encodeURIComponent(cargo.value);
				objetoAjax1_2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				objetoAjax1_2.send(datos2);
				break;
			case 404: // no se encontró el recurso
				cargo.innerHTML = "No es posible recuperar los cargos <br> Error 404 :"+this.statusText;
				break;
			default:
				cargo.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
	agregaEvento("change", cargo, actCargo);
}
function rellenarVotantes(){
	var votantes = document.getElementById("votantes");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				votantes.innerHTML = objetoAjax2.responseText;
				//No recogía los valores de min y max de los option del select de los rangos
				var votantes = document.getElementById("votantes");
				if (votantes.value==1){
					var min=0;
					var max=500000;
				}		
				if (votantes.value==2){
					var min=500000;
					var max=1000000;
				}
				if (votantes.value==3){
					var min=1000000;
					var max=1500000;
				}
				if (votantes.value==4){
					var min=1500000;
					var max=2147483647;
				}
				objetoAjax2_1.open("POST", "php/obtener_votant_num_vot.php", true);
				var datos3="min='"+encodeURIComponent(min)+"'&max='"+encodeURIComponent(max)+"'";
				objetoAjax2_1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				objetoAjax2_1.send(datos3);
				break;
			case 404: // no se encontró el recurso
				votantes.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				votantes.innerHTML = "Error " + this.status + ": ";
				break;
		}
	}
	agregaEvento("change", votantes, actVotantes);
}
function resultadosCargos(){
	var cargos = document.getElementById("resultadosCargo"); 
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjax1_2.responseText;
				var objeto_cargo = eval( '(' + datos_json + ')');
				var respuesta="Hay "+objeto_cargo.length+" registros";
				for (var i = 0; i < objeto_cargo.length; i++) {
					respuesta+="<p> "+objeto_cargo[i].responsable+" - "+objeto_cargo[i].cargo+"</p>";
				}
				cargos.innerHTML=respuesta;
				
				break;
			case 404: // no se encontró el recurso
				cargos.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				cargos.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
function resultadosVotantes(){
	var votantes = document.getElementById("resultadosVotantes"); 

	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjax2_1.responseText;
				//var objeto_votantes = eval( '(' + datos_json + ')');
				var objeto_votantes = JSON.parse(datos_json);
				var respuesta="Hay "+objeto_votantes.length+" registros";
				for (var i = 0; i < objeto_votantes.length; i++) {
					respuesta+="<p> "+objeto_votantes[i].responsable+" - "+objeto_votantes[i].num_votan+"</p>";
				}
				votantes.innerHTML=respuesta;

				break;
			case 404: // no se encontró el recurso
				votantes.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				votantes.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
function resultadosEmail(){
	var resultadosEmail = document.getElementById("resultadosEmail"); 
	var email = document.getElementById("email");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjax3.responseText;
				var objeto_emails = eval( '(' + datos_json + ')');
				var respuesta="Hay "+objeto_emails.length+" registros";
				for (var i = 0; i < objeto_emails.length; i++) {
					respuesta+="<p onclick='activar(this,"+objeto_emails[i].id+")'> "+objeto_emails[i].responsable+" - "+objeto_emails[i].email+"</p>";
				}
				resultadosEmail.innerHTML=respuesta;
				
				break;
			case 404: // no se encontró el recurso
				resultadosEmail.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				resultadosEmail.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
	agregaEvento("change", email, actEmail);
}
function activar(nombre,cod){
	var reset = document.getElementById("Limpiar Datos"); 
	reset.click();
	objetoAjaxRellena = crearAjax();
	objetoAjaxRellena.onreadystatechange = rellenarDatos;
	if (!objetoAjaxRellena) {
		alert("No se ha podido crear el objeto ajax relacionado con el cod");
		return;
	}
	objetoAjaxRellena.open("POST", "php/obtener_votant_id.php", true);
	try{
		objetoAjaxRellena.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="id="+encodeURIComponent(cod);
		objetoAjaxRellena.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function rellenarDatos(){
	var input0 = document.getElementById("updateID"); 
	var input1 = document.getElementById("updatejunta"); 
	var input2 = document.getElementById("updateresponsable"); 

	var input4 = document.getElementById("updateemail"); 
	var input5 = document.getElementById("updatenumvot"); 
	var input6 = document.getElementById("updatevotblanc"); 
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjaxRellena.responseText;
				//var datos = eval( '(' + datos_json + ')');
				var datos = JSON.parse(datos_json);
				for (var i = 0; i < datos.length; i++) {
					input0.value=datos[i].id;
					input1.value=datos[i].junta_electoral;
					input2.value=datos[i].responsable;
					obtenerCargo(datos[i].cargo);
					input4.value=datos[i].email;
					input5.value=datos[i].num_votan;
					input6.value=datos[i].num_blanco;
				}
				break;
			case 404: // no se encontró el recurso
				resultadosEmail.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				resultadosEmail.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
function obtenerCargo(id){
	objetoAjaxCargo = crearAjax();
	objetoAjaxCargo.onreadystatechange = obtenerCargo2;
	if (!objetoAjaxCargo) {
		alert("No se ha podido crear el objeto ajax relacionado con el cod");
		return;
	}
	objetoAjaxCargo.open("POST", "php/obtener_votant_cargo.php", true);
	try{
		objetoAjaxCargo.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="id_cargo="+encodeURIComponent(id);
		objetoAjaxCargo.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function obtenerCargo2(){
	var select_cargo = document.getElementById("updatecargo");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjaxCargo.responseText;
				var datos = JSON.parse(datos_json);
				for (var i = 0; i < datos.length; i++) {
					select_cargo.innerHTML="<option>"+datos[i].cargo+"</option>"
				}
				break;
			case 404: // no se encontró el recurso
				resultadosEmail.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				resultadosEmail.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
function grabarCambios(){
	var id = document.getElementById("updateID");
	var info = document.getElementById("updateDone");
	if (id.value=="")
		info.innerHTML="No hay nada que guardar";
	else{
		prepararGrabar();
	}
}
function prepararGrabar(){
	objetoAjaxUpdate = crearAjax();
	objetoAjaxUpdate.onreadystatechange = updateCambios;
	var info = document.getElementById("updateDone");
	var id = document.getElementById("updateID").value; 
	var junta_electoral = document.getElementById("updatejunta").value; 
	var responsable = document.getElementById("updateresponsable").value; 
	var cargo = document.getElementById("updatecargo").value;
	var email = document.getElementById("updateemail").value; 
	var num_votan = document.getElementById("updatenumvot").value; 
	var num_blanco = document.getElementById("updatevotblanc").value;
	if (!objetoAjaxUpdate) {
		alert("No se ha podido crear el objeto ajax relacionado con el cod");
		return;
	}
	objetoAjaxUpdate.open("POST", "php/updateRegistro.php", true);
	try{
		objetoAjaxUpdate.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="id="+encodeURIComponent(id)+"&junta_electoral="+encodeURIComponent(junta_electoral)+"&responsable="+encodeURIComponent(responsable)+"&cargo="+encodeURIComponent(cargo)+"&email="+encodeURIComponent(email)+"&num_votan="+encodeURIComponent(num_votan)+"&num_blanco="+encodeURIComponent(num_blanco);
		objetoAjaxUpdate.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function updateCambios(){
	var info = document.getElementById("updateDone");
	var filtro1 = document.getElementById("filtro1");
	var filtro2 = document.getElementById("filtro2");
	var filtro3 = document.getElementById("filtro3");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				var datos_json = objetoAjaxUpdate.responseText;
				if (datos_json.indexOf("true")!=-1)
					info.innerHTML="Los datos se han actualizdo ";
				//filtro3.innerHTML="";
				//filtro2.innerHTML="";
				//filtro1.innerHTML="";

				break;
			case 404: // no se encontró el recurso
				info.innerHTML = "No es posible recuperar los rangos <br> Error 404 :"+this.statusText;
				break;
			default:
				info.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
///////////////////////FIN ACT 5 - 6//////////////7