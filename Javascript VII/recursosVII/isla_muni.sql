-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-11-2016 a las 15:05:32
-- Versión del servidor: 5.6.29
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `isla_muni`
--
CREATE DATABASE IF NOT EXISTS `isla_muni` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `isla_muni`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxislas`
--

DROP TABLE IF EXISTS `auxislas`;
CREATE TABLE `auxislas` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `auxislas`
--

TRUNCATE TABLE `auxislas`;
--
-- Volcado de datos para la tabla `auxislas`
--

INSERT INTO `auxislas` (`id`, `descripcion`) VALUES
(1, 'Fuerteventura'),
(2, 'Gran Canaria'),
(3, 'Lanzarote'),
(4, 'La Gomera'),
(5, 'El Hierro'),
(6, 'La Palma'),
(7, 'Tenerife');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auxmunicipios`
--

DROP TABLE IF EXISTS `auxmunicipios`;
CREATE TABLE `auxmunicipios` (
  `isla` int(11) NOT NULL,
  `municipio` int(11) NOT NULL,
  `descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `auxmunicipios`
--

TRUNCATE TABLE `auxmunicipios`;
--
-- Volcado de datos para la tabla `auxmunicipios`
--

INSERT INTO `auxmunicipios` (`isla`, `municipio`, `descripcion`) VALUES
(2, 35001, 'Agaete'),
(2, 35002, 'Agüimes'),
(1, 35003, 'Antigua'),
(3, 35004, 'Arrecife'),
(2, 35005, 'Artenara'),
(2, 35006, 'Arucas'),
(1, 35007, 'Betancuria'),
(2, 35008, 'Firgas'),
(2, 35009, 'Gáldar'),
(3, 35010, 'Haría'),
(2, 35011, 'Ingenio'),
(2, 35012, 'Mogán'),
(2, 35013, 'Moya'),
(1, 35014, 'Oliva, La'),
(1, 35015, 'Pájara'),
(2, 35016, 'Palmas de Gran Canaria, Las'),
(1, 35017, 'Puerto del Rosario'),
(3, 35018, 'San Bartolomé'),
(2, 35019, 'San Bartolomé de Tirajana'),
(2, 35020, 'Aldea de San Nicolás, La'),
(2, 35021, 'Santa Brígida'),
(2, 35022, 'Santa Lucía de Tirajana'),
(2, 35023, 'Santa María de Guía de Gran Canaria'),
(3, 35024, 'Teguise'),
(2, 35025, 'Tejeda'),
(2, 35026, 'Telde'),
(2, 35027, 'Teror'),
(3, 35028, 'Tías'),
(3, 35029, 'Tinajo'),
(1, 35030, 'Tuineje'),
(2, 35031, 'Valsequillo de Gran Canaria'),
(2, 35032, 'Valleseco'),
(2, 35033, 'Vega de San Mateo'),
(3, 35034, 'Yaiza'),
(7, 38001, 'Adeje'),
(4, 38002, 'Agulo'),
(4, 38003, 'Alajeró'),
(7, 38004, 'Arafo'),
(7, 38005, 'Arico'),
(7, 38006, 'Arona'),
(6, 38007, 'Barlovento'),
(6, 38008, 'Breña Alta'),
(6, 38009, 'Breña Baja'),
(7, 38010, 'Buenavista del Norte'),
(7, 38011, 'Candelaria'),
(7, 38012, 'Fasnia'),
(5, 38013, 'Frontera'),
(6, 38014, 'Fuencaliente de la Palma'),
(7, 38015, 'Garachico'),
(6, 38016, 'Garafía'),
(7, 38017, 'Granadilla de Abona'),
(7, 38018, 'Guancha, La'),
(7, 38019, 'Guía de Isora'),
(7, 38020, 'Güímar'),
(4, 38021, 'Hermigua'),
(7, 38022, 'Icod de los Vinos'),
(7, 38023, 'San Cristóbal de La Laguna'),
(6, 38024, 'Llanos de Aridane, Los'),
(7, 38025, 'Matanza de Acentejo, La'),
(7, 38026, 'Orotava, La'),
(6, 38027, 'Paso, El'),
(7, 38028, 'Puerto de la Cruz'),
(6, 38029, 'Puntagorda'),
(6, 38030, 'Puntallana'),
(7, 38031, 'Realejos, Los'),
(7, 38032, 'Rosario, El'),
(6, 38033, 'San Andrés y Sauces'),
(7, 38034, 'San Juan de la Rambla'),
(7, 38035, 'San Miguel de Abona'),
(4, 38036, 'San Sebastián de la Gomera'),
(6, 38037, 'Santa Cruz de la Palma'),
(7, 38038, 'Santa Cruz de Tenerife'),
(7, 38039, 'Santa Úrsula'),
(7, 38040, 'Santiago del Teide'),
(7, 38041, 'Sauzal, El'),
(7, 38042, 'Silos, Los'),
(7, 38043, 'Tacoronte'),
(7, 38044, 'Tanque, El'),
(6, 38045, 'Tazacorte'),
(7, 38046, 'Tegueste'),
(6, 38047, 'Tijarafe'),
(5, 38048, 'Valverde'),
(4, 38049, 'Valle Gran Rey'),
(4, 38050, 'Vallehermoso'),
(7, 38051, 'Victoria de Acentejo, La'),
(7, 38052, 'Vilaflor'),
(6, 38053, 'Villa de Mazo'),
(5, 38901, 'Pinar de El Hierro, El');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auxislas`
--
ALTER TABLE `auxislas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `auxmunicipios`
--
ALTER TABLE `auxmunicipios`
  ADD PRIMARY KEY (`municipio`),
  ADD KEY `Isla` (`isla`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auxislas`
--
ALTER TABLE `auxislas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
