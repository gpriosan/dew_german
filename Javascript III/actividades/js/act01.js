info=""+navigator.appCodeName+"<br>";
info+=navigator.appName+"<br>";
info+=navigator.appVersion+"<br>";
info+=navigator.platform+"<br>";
info+=navigator.plugins+"<br>";

if (navigator.userAgent.indexOf("Mozilla")!=-1){
	info+="El user agent del navegador es Mozilla<br>";
}
else if (navigator.userAgent.indexOf("Opera")!=-1){
	info+="El user agent del navegador es Opera<br>";
}
else if (navigator.userAgent.indexOf("Chrome")!=-1){
	info+="El user agent del navegador es Chrome<br>";
}
else if (navigator.userAgent.indexOf("MSIE")!=-1){
	info+="El user agent del navegador es Internet Explorer<br>";
}
else{
	info+="El user agent del navegador es Desconocido<br>";
}


if (navigator.cookieEnabled)
	info+="El navegador tiene habilitadas las cookies<br>";
else
	info+="El navegador no tiene habilitadas las cookies<br>";
info+=navigator.language+"<br>";
if (navigator.onLine)
	info+="El navegador puede conectarse a internet<br>";
else
	info+="El navegador no puede conectarse a internet<br>";
info+=navigator.product+"<br>";
if (navigator.javaEnabled())
	info+="Se permite java<br>";
else
	info+="El navegador no permite java<br>";

contenedor=document.getElementById("container");
contenedor.innerHTML=info;