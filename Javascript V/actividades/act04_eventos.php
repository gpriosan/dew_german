<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="DEW - Eventos ">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="Germán">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit-no">
	<noscript>
		Habilita javascript para visualizar correctamente la página
	</noscript>
	<title>DEW - Eventos - Act 4</title>
	
	<link rel="stylesheet" href="css/bootstrap-grid.min.css"/>
	<link rel="stylesheet" href="css/bootstrap-reboot.min.css"/>
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<style>
		.container{margin: 6em;}	
		input[type="submit"],#aceptar,input[type="reset"]{
			background-color: orange;
			color:white;
		}
		#info{
			text-align: center;
			background: #c3c3c3;
			color:red;
			padding:1em;
		}
		input[type="text"]{
			width: 15em !important;
		}
		input[type="password"]{
			width: 15em !important;
		}
		input[type="email"]{
			width: 15em !important;
		}
		input[type="option"]{
			width: 15em !important;
		}
		select{
			width: 15em !important;
		}
	</style>
</head>
<body>
	<?php
		include('php/conexion.php');
	?>
	<div id="fecha"></div>
	<div id="datos"></div>
	
	<h3 class="text-center">Javascript V - Actividad 4 - Eventos</h3>
    <p></p>
	<div class="container border">
		<div id="info">
			Información adicional
		</div>
		<!-- <form action="" onsubmit="return validarDatos(this)" method="get" name="formulario" id="formulario"> -->
		<form  method="get" name="formulario" id="formulario">
			<div class="form-group row">
				<div class="col-sm-6">Nick</div>
				<div class="col-sm-6">
					<input type="text" name="nick" id="nick">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Contraseña</div>
				<div class="col-sm-6">
					 <input type="password" class="form-control"  name="contraseña" id="contraseña" placeholder="Escriba el Password">

				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Repita Contraseña</div>
				<div class="col-sm-6">
					<input type="password" class="form-control"  name="Rcontraseña" id="Rcontraseña" placeholder="Password">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Nombre</div>
				<div class="col-sm-6">
					<input type="text" name="nombre" id="nombre">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Apellidos</div>
				<div class="col-sm-6">
					<input type="text" name="apellidos" id="apellidos">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Email</div>
				<div class="col-sm-6">
					<input type="email" name="email" id="email">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">Comunidad</div>
				<div class="col-sm-6">
					<select class="form-control" name="comunidades" id="comunidades_id">
						<?php
						try{
							$query = $con->query("select comunidad from auxComunidades");
							if ($resultado = $con->query("select comunidad from auxComunidades")) {
								$contador=1;
						    	while ($fila = $resultado->fetch_row()) {
						    		echo "<option value='$contador' name='$fila[0]' id='$fila[0]'>".$fila[0]."</option>";
						    		$contador=$contador+1;
						    	}
						    }

						}
						catch(PDOException $e){
							echo "<option>ERROR".$e->getMessage."</option>";
						} 
						?>
					</select>
				</div>
				
			</div>
			<div class="form-group row">
					<div class="col-sm-6">
						<input type="text" name="codigo" id="codigo" value="">
					</div>
					<div class="col-sm-6">
						<input type="button" name="activarC" id="activarC" value="activar comunidad">
						<input type="button" name="desactivarC" id="desactivarC" value="desactivar comunidad">
					</div>
				</div>
			<div class="form-group row">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">INTERESES:</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">
				<input type="button" name="todos" id="todos" value="todos">
				<input type="button" name="ninguno" id="ninguno" value="ninguno">
					
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="programacion" id="programacion">
				      Programación
				    </label>
				  </div>
				</div>
				<div class="col-sm-6">
					
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="diseño_web" id="diseño_web">
				      Diseño web
				    </label>
				  </div>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="redes" id="redes">
				      Redes
				    </label>
				  </div>
				</div>
				<div class="col-sm-6"></div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">CURSO:</div>
			</div>
			<div class="form-group row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1daw" value="option2">
					    1º Desarrollo Web
					  </label>
					
				</div>
				
					<div class="form-check">
						
						  <label class="form-check-label">
						    <input class="form-check-input" type="radio" name="curso" id="2daw" value="option2">
						    2º Desarrollo Web
						  </label>
					
					</div>
				
			</div>
			<div class="form-group row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1dam" value="option2">
					    1º Multiplataforma
					  </label>
					
				</div>
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="2dam" value="option2">
					    2º Multiplataforma
					  </label>
					
				</div>
				
			</div>
			<div class="form-group row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1asir" value="option2">
					    1º Admin Sist
					  </label>
					
				</div>

				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="2asir" value="option2">
					    2º Admin Sist
					  </label>
					
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6"></div>
				<div class="col-sm-6">Nivel:</div>
			</div>
			<div class="row">
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="inicial" value="option2">
					    Inicial
					  </label>
					
				</div>
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="medio" value="option2">
					  Medio
					  </label>
					
				</div>
			</div>
			<div class="form-group row">
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="avanzado" value="option2">
					    Avanzado
					  </label>
					
				</div>
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="experto" value="option2">
					    Experto
					  </label>
					
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6">
					<!--<input type="submit" value="Aceptar" name="aceptar" id="aceptar">-->
					<input type="button" value="Aceptar" name="aceptar" id="aceptar">
				</div>
				<div class="col-sm-6">
					<input type="reset" value="Restablecer" name="Restablecer" id="Restablecer">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-6"></div>
				<div class="col-sm-6"></div>
			</div>
		</form>

	</div>

<script type="text/javascript" src="js/slim.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/act04.js"></script>

</body>
</html>