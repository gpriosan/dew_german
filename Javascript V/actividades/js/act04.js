function lastMod(){
	var modDate = new Date(document.lastModified);
	var hoy = new Date();
	var modYear = modDate.getYear();
	modYear+=1900;
	fecha=("Última modificación del archivo: "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (modYear+"<br>"));
	var año = hoy.getYear();
	año+=1900;
	fecha+=("Estamos a : "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (año+""));
	document.getElementById('fecha').innerHTML=fecha;
}
function preparar(){
	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	var datos=document.getElementById('datos');
	for (var i = 0; i < elementos_del_formulario.length; i++) {
		//datos.innerHTML+="name: "+elementos_del_formulario[i].name+"--ID: "+elementos_del_formulario[i].id+"-- Value: "+elementos_del_formulario[i].value+"--Type: "+elementos_del_formulario[i].type+" Checked: "+elementos_del_formulario[i].checked+"<br>";

		if (elementos_del_formulario[i].type=="password"){
			elementos_del_formulario[i].style.background="#C7C7C7";
		}
		if (elementos_del_formulario[i].type=="text"){
			elementos_del_formulario[i].style.color="red";
		}
		if (elementos_del_formulario[i].type=="checkbox"){
			elementos_del_formulario[i].checked=0;
			elementos_del_formulario[i].value="off";
		}
		if (elementos_del_formulario[i].type=="radio"){
			elementos_del_formulario[i].style.margin="10px";
		}
		if ( (elementos_del_formulario[i].type=="submit") || (elementos_del_formulario[i].type=="reset")){
			elementos_del_formulario[i].style.color="white";
		}
	}
	
}
function seleccionar(){
	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	//var datos=document.getElementById('datos');
	for (var i = 0; i < elementos_del_formulario.length; i++) {
		if (elementos_del_formulario[i].type=="checkbox"){
			elementos_del_formulario[i].checked=1;
			//elementos_del_formulario[i].value="on";
		}
	}
}
function deseleccionar(){
	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	//var datos=document.getElementById('datos');
	for (var i = 0; i < elementos_del_formulario.length; i++) {
		if (elementos_del_formulario[i].type=="checkbox"){
			elementos_del_formulario[i].checked=0;
			//elementos_del_formulario[i].value="off";
		}
	}
}
function cambioCurso(curso){
	var cad="";
	var txt="de 'http://localhost:8080'";
	cad+=txt.bold();
	txt="\nHay 6 cursos\nEl curso seleccionado es: "+curso.id+"";
	cad+=txt;
	alert(cad);

	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	if (curso.id[0]=="2"){
		for (var i = 0; i < elementos_del_formulario.length; i++) {
			if ((elementos_del_formulario[i].type=="radio") && (elementos_del_formulario[i].name=="nivel") ){
				if (elementos_del_formulario[i].id=="medio")
				elementos_del_formulario[i].checked=1;
			}
			
		}
	}
	if (curso.id[0]=="1"){
		for (var i = 0; i < elementos_del_formulario.length; i++) {
			if ((elementos_del_formulario[i].type=="radio") && (elementos_del_formulario[i].name=="nivel") ){
				if (elementos_del_formulario[i].id=="inicial")
				elementos_del_formulario[i].checked=1;
			}
			
		}
	}
}
function estiloCebra(){
	var comunidades=document.getElementsByTagName('option');
	for (var i = 0; i < comunidades.length; i++) {
		if (i%2==0)
			comunidades[i].style.background=" #C7C7C7";
	}
}
function desactivaOpciones(caracter){
	var comunidades=document.getElementsByTagName('option');
	for (var i = 0; i < comunidades.length; i++) {
		if (comunidades[i].id[0]==caracter)
			comunidades[i].disabled=1;
	}
}
function activarComunidad(){
	var codigo = document.getElementById('codigo').value;
	var comunidades=document.getElementsByTagName('option');
	for (var i = 0; i < comunidades.length-1; i++) {
		if (comunidades[i].value==codigo){
			comunidades[i].disabled=0;
		}
	}
}
function desactivarComunidad(){
	var codigo = document.getElementById('codigo').value;
	var comunidades=document.getElementsByTagName('option');
	for (var i = 0; i < comunidades.length-1; i++) {
		if (comunidades[i].value==codigo){
			comunidades[i].disabled=1;
		}
	}
}
function agregaEvento(evento, objeto, funcion) {
	if (objeto.addEventListener) //estamos en un DOM del estándar
		objeto.addEventListener(evento,funcion,false);
	else if (objeto.attachEvent) // estamos en un DOM de IExplorer
		objeto.attachEvent("on"+evento, funcion);
	else // no estamos en un DOM ni W3C ni de IExplorer? modelo tradicional
		objeto["on"+evento] = funcion; // recuerda: objetos son arrays asociativos
}
function validarEmail(email) {
 return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email));
}
function validarNick(valor) {
  if (/^([a-z]([a-z0-9\-_]*))$/.test(valor)){
	   return true;
	  } 
  else {
	   return false;
	  }
}
function validarDatos(){
	var elementos=formulario.elements;//recoge el formulario por el id
	valida=true;
	valida_checbox=false;
	valida_radio=false;
	var info = document.getElementById('info');
	var pass = document.getElementById('contraseña');
	var rpass = document.getElementById('Rcontraseña');
	var email = document.getElementById('email');
	var nick = document.getElementById('nick');

	info.innerHTML="";
	for (var i = 0; i < elementos.length; i++) {
		if (elementos[i].id=="codigo")
			valida=true;
		else{
			if (elementos[i].value==""){
				elementos[i].focus();
				valida=false;
				info.innerHTML+="Falta de  rellenar : "+elementos[i].id+"<br>";
			}
			if (elementos[i].type=="checkbox"){
				if (elementos[i].checked==true){
					valida_checbox=true;
				}
			}
			if (elementos[i].type=="radio"){
				if (elementos[i].checked==true){
					valida_radio=true;
				}
			}
		}

	}
	if (valida_checbox==false){
		info.innerHTML+="Falta de  marcar algún interés<br>";
		valida=false;
	}
	if (valida_radio==false){
		info.innerHTML+="Falta de  marcar algún curso<br>";
		valida=false;
	}
	if (pass.value!=rpass.value){
		info.innerHTML+="Las contraseñas no  son iguales<br>";
		valida=false;

	}
	validacion_email= validarEmail(email.value);
	if (validacion_email==false){
		valida=false;
		info.innerHTML+="La dirección de email no es correcta<br>";
	}
	validacion_nick= validarNick(nick.value);
	if (validacion_nick==false){
		valida=false;
		info.innerHTML+="El nick  no es correcto: Ej: german-_4<br>";

	}
		
	return valida;

}
function estaVacio(campo){
	if (campo.value=="")
		campo.focus();

}

function configurarEventos() {
	function llamarCambiarCurso(){
		cambioCurso(this);
	}
	function llamarEstaVacio(){
		estaVacio(this);
	}
	
	estiloCebra();
	desactivaOpciones("C");
	lastMod();
	preparar();
	activar_comunidad=document.getElementById('activarC');
	desactivar_comunidad=document.getElementById('desactivarC');
	btnTodos=document.getElementById('todos');
	btnNinguno=document.getElementById('ninguno');
	var input=document.getElementsByTagName('input');
	btnSubmit=document.getElementById('formulario');
	btnAceptar=document.getElementById('aceptar');
	for (var i = 0; i < input.length; i++) {
		if (input[i].name=="curso"){
			agregaEvento("change",input[i],llamarCambiarCurso); //Se crea la funcion intermedi llamarCambiarCurso para enviar this(El radio que se pulsa)
		}
		agregaEvento("blur",input[i],llamarEstaVacio);
	}
	agregaEvento("click",activar_comunidad,activarComunidad);
	agregaEvento("click",desactivar_comunidad,desactivarComunidad);
	agregaEvento("click",btnTodos,seleccionar);
	agregaEvento("click",btnNinguno,deseleccionar);
	agregaEvento("click",btnAceptar,validarDatos);// llama a validarDatos y allí recoge los elementois del formulario
	
}
window.onload=configurarEventos;