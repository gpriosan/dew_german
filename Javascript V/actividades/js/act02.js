function agregaEvento(evento, objeto, funcion) {
	if (objeto.addEventListener) //estamos en un DOM del estándar
		objeto.addEventListener(evento,funcion,false);
	else if (objeto.attachEvent) // estamos en un DOM de IExplorer
		objeto.attachEvent("on"+evento, funcion);
	else // no estamos en un DOM ni W3C ni de IExplorer? modelo tradicional
		objeto["on"+evento] = funcion; // recuerda: objetos son arrays asociativos
}

window.onload	=	function()	{	
	document.getElementById("principal").addEventListener("click",function(){			
	alert("Pulsado	el	contenedor	principal");			
	},true);	//	estamos	utilizando	burbujeo			
	document.getElementById("secundario").addEventListener("click",function(){	
	alert("Pulsado	el	contenedor	secundario");			
	},true);	//	estamos	utilizando	burbujeo	
	document.getElementById("miboton").addEventListener("click",function(){
	alert("Pulsado	el	botón");			
	},true);	//	estamos	utilizando	burbujeo	
}	












function lastMod(){
	var modDate = new Date(document.lastModified);
	var hoy = new Date();
	var modYear = modDate.getYear();
	modYear+=1900;
	fecha=("Última modificación del archivo: "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (modYear+"<br>"));
	var año = hoy.getYear();
	año+=1900;
	fecha+=("Estamos a : "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (año+""));
	document.getElementById('fecha').innerHTML=fecha;
}
window.onload=lastMod;