<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Actividad 2 Ventana - Pantalla - Despedida">
	<meta name="keywords" content="HTML,CSS,XML,JavaScript">
	<meta name="author" content="Germán">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit-no">
	<noscript>Habilita javascript para visualizar correctamente la página
	</noscript>
	<title>DEW - Actividad 1 - Comunidades</title>
	
	<link rel="stylesheet" href="css/bootstrap-grid.min.css"/>
	<link rel="stylesheet" href="css/bootstrap-reboot.min.css"/>
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<style>
		.container{margin: 6em;}	
		input[type="submit"]{
			background-color: orange;
		}
		input[type="reset"]{
			background-color: orange;
		}
	</style>
<script>
function lastMod(){
	var modDate = new Date(document.lastModified);
	var hoy = new Date();
	var modYear = modDate.getYear();
	modYear+=1900;
	fecha=("Última modificación del archivo: "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (modYear+"<br>"));
	var año = hoy.getYear();
	año+=1900;
	fecha+=("Estamos a : "+ modDate.getDate() + "/" + (modDate.getMonth()+1) + "/" + (año+""));
	document.getElementById('fecha').innerHTML=fecha;
}
function preparar(){
	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	var datos=document.getElementById('datos');
	for (var i = 0; i < elementos_del_formulario.length; i++) {
		datos.innerHTML+="name: "+elementos_del_formulario[i].name+"--ID: "+elementos_del_formulario[i].id+"-- Value: "+elementos_del_formulario[i].value+"--Type: "+elementos_del_formulario[i].type+" Checked: "+elementos_del_formulario[i].checked+"<br>";
		datos.innerHTML="";

		if (elementos_del_formulario[i].type=="password"){
			elementos_del_formulario[i].style.background="#C7C7C7";
		}
		if (elementos_del_formulario[i].type=="text"){
			elementos_del_formulario[i].style.color="red";
		}
		if (elementos_del_formulario[i].type=="checkbox"){
			elementos_del_formulario[i].checked=0;
			elementos_del_formulario[i].value="off";

		}
		if (elementos_del_formulario[i].type=="radio"){
			elementos_del_formulario[i].style.margin="10px";
		}
		if ( (elementos_del_formulario[i].type=="submit") || (elementos_del_formulario[i].type=="reset")){
			elementos_del_formulario[i].style.color="white";
		}
	}
	
}
function seleccionar(eleccion){
	var form1=document.forms[0];
	var elementos_del_formulario=form1.elements;
	var datos=document.getElementById('datos');
	
	for (var i = 0; i < elementos_del_formulario.length; i++) {
		if (eleccion=="todos"){
			if (elementos_del_formulario[i].type=="checkbox"){
				elementos_del_formulario[i].checked=1;
				elementos_del_formulario[i].value="on";
			}
	
		}
		else if (eleccion=="ninguno"){
			if (elementos_del_formulario[i].type=="checkbox"){
				elementos_del_formulario[i].checked=0;
				elementos_del_formulario[i].value="off";
			}
		}
	}
}

	</script>
</head>
<body onload="lastMod(),preparar()">
	<?php
		include('php/conexion.php');
	?>
	<div id="fecha"></div>
	<div id="datos"></div>
	
	<h3 class="text-center">Actividad 1 - Comunidades</h3>
    <p></p>
	<div class="container">
		<form action="" name="">
			<div class="row">
				<div class="col-4">Nick</div>
				<div class="col-4">
					<input type="text" name="nick" id="nick">
				</div>
			</div>
			<div class="row">
				<div class="col-4">Contraseña</div>
				<div class="col-4">
					 <input type="password" class="form-control"  name="contraseña" id="contraseña" placeholder="Password">

				</div>
			</div>
			<div class="row">
				<div class="col-4">Repita Contraseña</div>
				<div class="col-4">
					<input type="password" class="form-control"  name="Rcontraseña" id="Rcontraseña" placeholder="Password">
				</div>
			</div>
			<div class="row">
				<div class="col-4">Nombre</div>
				<div class="col-4">
					<input type="text" name="nombre" id="nombre">
				</div>
			</div>
			<div class="row">
				<div class="col-4">Apellidos</div>
				<div class="col-4">
					<input type="text" name="apellidos" id="apellidos">
				</div>
			</div>
			<div class="row">
				<div class="col-4">Email</div>
				<div class="col-4">
					<input type="text" name="email" id="email">
				</div>
			</div>
			<div class="row">
				<div class="col-4">Comunidad</div>
				<div class="col-4">
					<select class="form-control" name="comunidades" id="comunidades_id">
						<?php
						try{
							$query = $con->query("select comunidad from auxComunidades");
							if ($resultado = $con->query("select comunidad from auxComunidades")) {
								$contador=1;
						    	while ($fila = $resultado->fetch_row()) {
						    		echo "<option value='$contador' name='$fila[0]' id='$fila[0]'>".$fila[0]."</option>";
						    		$contador=$contador+1;
						    	}
						    }

						}
						catch(PDOException $e){
							echo "<option>ERROR".$e->getMessage."</option>";
						} 
						?>
					</select>
				</div>
				
			</div>
			<div class="row">
				<div class="col-4">
					<input type="text" name="codigo" id="codigo" value="">
				</div>
				<div class="col-4">
					<input type="button" name="activarC" id="activarC" onclick="activarComunidad()" value="activar comunidad">
				</div>
			</div>
		<div class="border border-dark m-2" style="width: 70%;">	
			<div class="row">
				<div class="col-4">
					<h3>Agregar Comunidad: </h3>
				</div>
				<div class="col-4">
					<input type="button" name="agregar" id="agregar" value="Agregar" onclick="agregarComunidad()">
				</div>
			</div>	
			<div class="row">	
				<div class="col-4">
					<p>Posición: </p>
				</div>
				<div class="col-4">
					<select class='form-control' name='posicion' id='posicion'>

					</select>
				</div>
			</div>	
			<div class="row">		
				<div class="col-4">
					<p>Valor: </p>
				</div>
				<div class="col-4">
					<select class='form-control' name='valor' id='valor'>
						
					</select>	
				</div>
			</div>	
			<div class="row">		
				<div class="col-4">
					<p>Texto: </p>
				</div>
				<div class="col-4">
					<input type="text" name="texto" id="texto">	
				</div>
			</div>
		</div>	
		<div class="border border-dark m-2"  style="width: 70%;">
			<div class="row">
				<div class="col-4">
					<h3>Borrar Comunidad: </h3>
				</div>
				<div class="col-4">
					<input type="button" name="borrar" id="borrar" value="Borrar" onclick="borrarComunidad()">
				</div>
			</div>
			<div class="row">		
				<div class="col-4">
					<p>Con el Texto: </p>
				</div>
				<div class="col-4">
					<input type="text" name="texto_borrar" id="texto_borrar">	
				</div>
			</div>
		</div>	
			<div class="row">
				<div class="col-4"></div>
				<div class="col-4">INTERESES:</div>
				<input type="button" name="todos" id="todos" value="todos" onclick="seleccionar('todos')">
				<input type="button" name="ninguno" id="ninguno" value="ninguno" onclick="seleccionar('ninguno')">
			</div>
			<div class="row">
				<div class="col-4">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="programacion" id="programacion">
				      Programación
				    </label>
				  </div>
				</div>
				<div class="col-4">
					
				</div>
			</div>
			<div class="row">
				<div class="col-4">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="diseño_web" id="diseño_web">
				      Diseño web
				    </label>
				  </div>
				</div>
				<div class="col-4"></div>
			</div>
			<div class="row">
				<div class="col-4">
					  <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input" name="redes" id="redes">
				      Redes
				    </label>
				  </div>
				</div>
				<div class="col-4"></div>
			</div>
			<div class="row">
				<div class="col-4"></div>
				<div class="col-4">CURSO:</div>
			</div>
			<div class="row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1daw" value="option2" onchange="cambioCurso(this)">
					    1º Desarrollo Web
					  </label>
					
				</div>
				
					<div class="form-check">
						
						  <label class="form-check-label">
						    <input class="form-check-input" type="radio" name="curso" id="2daw" value="option2" onchange="cambioCurso(this)">
						    2º Desarrollo Web
						  </label>
					
					</div>
				
			</div>
			<div class="row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1dam" value="option2" onchange="cambioCurso(this)">
					    1º Multiplataforma
					  </label>
					
				</div>
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="2dam" value="option2" onchange="cambioCurso(this)">
					    2º Multiplataforma
					  </label>
					
				</div>
				
			</div>
			<div class="row">
				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="1asir" value="option2" onchange="cambioCurso(this)">
					    1º Admin Sist
					  </label>
					
				</div>

				<div class="form-check">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="curso" id="2asir" value="option2" onchange="cambioCurso(this)">
					    2º Admin Sist
					  </label>
					
				</div>
			</div>
			<div class="row">
				<div class="col-4"></div>
				<div class="col-4">Nivel:</div>
			</div>
			<div class="row">
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="inicial" value="option2">
					    Inicial
					  </label>
					
				</div>
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="medio" value="option2">
					  Medio
					  </label>
					
				</div>
			</div>
			<div class="row">
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="avanzado" value="option2">
					    Avanzado
					  </label>
					
				</div>
				<div class="form-check2">
					
					  <label class="form-check-label">
					    <input class="form-check-input" type="radio" name="nivel" id="experto" value="option2">
					    Experto
					  </label>
					
				</div>
			</div>
			<div class="row">
				<div class="col-4">
					<input type="submit" value="Aceptar" name="aceptar" id="aceptar">
				</div>
				<div class="col-4">
					<input type="reset" value="Restablecer" name="Restablecer" id="Restablecer">
				</div>
			</div>
			<div class="row">
				<div class="col-4"></div>
				<div class="col-4"></div>
			</div>
		</form>

	</div>

<script type="text/javascript" src="js/slim.js"></script>
<script type="text/javascript" src="js/popper.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/act01.js"></script>

</body>
</html>