contenedor=document.getElementById("container");
datos=document.getElementsByClassName("info")[0];
//contenedor.innerHTML=info;
var ventana=window;
var ancho=400;
var alto=400;
function centrarVentana(ventana){
	datos.innerHTML="";
	xpos=((screen.width/2)-(ancho/2));
	ypos=((screen.height/2)-(alto/2));
	ventana.moveTo(xpos,ypos);
}
function nuevaVentana(url,nombre,ancho,alto){
	datos.innerHTML="";
	xpos=(screen.width/2)-(ancho/2); 
	ypos=(screen.height/2)-(alto/2); 
	ventana=window.open(url,nombre,'resizable=1,width='+ancho+',height='+alto+',left='+xpos+',top='+ypos+'');
}
function nuevaVentana2(nombre,ancho,alto){
	datos.innerHTML="";
	xpos=(screen.width/2)-(ancho/2); 
	ypos=(screen.height/2)-(alto/2); 
	ventana=window.open("",nombre,'resizable=1,width='+ancho+',height='+alto+',left='+xpos+',top='+ypos+'');
}
function cerrarVentana(){
	datos.innerHTML="";
	if ((typeof ventana.close()==='undefined'))
		datos.innerHTML=("La ventana está cerrada o no se ha abierto");
	if(ventana=='undefined')
		datos.innerHTML=("La ventana está cerrada o no se ha abierto");
	else
		ventana.close();
	
}
function ampliarVentana(){
	datos.innerHTML="";
	xpos=ventana.screenX;
	ypos=ventana.screenY;
	if ((ventana.screen.width>ancho) && (xpos>(xpos-ancho+300))){
		ancho+=300;
		alto+=300;
		ventana.resizeTo(ancho,alto);
		console.log(""+ventana.screen.width+"----"+ventana.screen.height+"---"+xpos+"---"+ypos+"----"+ancho+"----"+alto);
		//centrarVentana(ventana)
	}
	else{
		datos.innerHTML=("No se puede aumentar más");
		console.log(""+ventana.screen.width+"----"+ventana.screen.height+"---"+xpos+"---"+ypos+"----"+ancho+"----"+alto);
	}
}
function reducirVentana(){
	datos.innerHTML="";
	if (ancho>100){
		ventana.resizeTo(ancho-100,alto-100);
		ancho-=100;
		alto-=100;
		//centrarVentana(ventana)
	}
	else{
		datos.innerHTML=("No se puede reducir más");
	}
}
function moverVentana(){
	datos.innerHTML="";
	xpos=parseInt(document.getElementById('posx').value);
	ypos=parseInt(document.getElementById('posy').value);
	if ( (xpos>ventana.screen.width) || (ypos>ventana.screen.height) ){
		datos.innerHTML=("El ancho o el alto no encaja");
	}
	else{
		ventana.moveTo(xpos,ypos);
	}
}