
function Persona(nombre,apellidos,edad){
	"use strict";
	this.nombre=nombre || "Desconocido";
	this.apellidos=apellidos || "Desconocido";
	this.edad=+edad || 0;
}
	Persona.prototype={
		setNombre: function(nombre){
			this.nombre=nombre;
		},
		setApellidos: function(apellidos){
			this.apellidos=apellidos;
		},
		setEdad: function(edad){
			this.edad=edad;
		},
		speak:function (mensaje) {
			mensaje=mensaje ? ", " + mensaje: "";
			return "Nombre: "+this.name + mensaje;
		}
	};
	//@override
	Persona.prototype.toString=function(){
		cad="Persona: "+this.nombre+" "+ this.apellidos+" "+this.edad+" " ;
		return cad;
	};

	function Profesor(especialidad,modulos){
		"use strict";
		this.especialidad=especialidad || "Desconocido";
		this.modulos=[] || "Desconocido";
	}
		Profesor.prototype=Object.create(Persona.prototype);
		Profesor.prototype.constructor=Profesor;
		Profesor.prototype.setEspecialidad=function(especialidad){
			this.especialidad=especialidad;
		};
		Profesor.prototype.addModulo=function(modulo){
			this.modulos.push(modulo);
		};
		//@override
		Profesor.prototype.toString=function(){
			cad="Profesor: "+this.nombre+" "+ this.apellidos+" "+this.edad+" "+this.especialidad+" "+this.modulos ;
			return( cad);
		};
	


var alumno=new Persona("Pepe","lópez",12);
var profesor=new Profesor();

profesor.setEdad(40);
profesor.setNombre("Paco");
profesor.setApellidos("Gutierrez");
profesor.setEspecialidad("Desarrolo");
profesor.addModulo("DPL");
profesor.addModulo("DEW");

cad=profesor.toString()+"<br>";
cad+=alumno.toString();
datos=document.getElementsByClassName("info")[0];
datos.innerHTML=cad;

