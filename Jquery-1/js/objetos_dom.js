/**
###########################Selectores##################

$("p:first") // selecciona el primer párrafo
$("p:eq(0)") // selecciona el primer párrafo, aquél
cuya posición es igual a cero (el
primero).
$("p:eq(2)") // selecciona el tercer párrafo, aquél cuya posición es igual a dos.
$("p:odd") // selecciona todos los párrafos en posiciones impares.
$("p:even") // selecciona todos los párrafos en posiciones pares.
$("p:gt(3)") // selecciona todos los párrafos en posiciones mayores a 3 (del 4o en
adelante).
$("p:lt(2)") //
selecciona todos los párrafos en posiciones menores a 2 (los dos
primeros).
$("td:contains("Enrique")")
// selecciona todas las celdas de tabla que contienen
"Enrique"
$("p:not(.rotulo)") //
selecciona todos los párrafos que no tienen la clase rótulo.


###########################Selectores##################
**/

/**
###########################Selectores para formularios##################

:button selecciona los elementos <button> y los input type="button"
:checkbox selecciona todos los elementos type="checkbox"
:checked selecciona todos los inputs con estado checked
:disabled selecciona todos los elementos de formulario con estado disabled
:enabled selecciona todos los elementos de formulario sin estado disabled
:file selecciona los input type="file"
:image selecciona los input type="image"
:input selecciona los <input>, <textarea>, and <select>
:password selecciona los input type="password"
:radio selecciona los input type="radio"
:reset y :submit selecciona los input type="reset" o los input typ=”submit”
:selected selecciona todas aquellos <option> cuyo estado es “selected”
:text seleccciona los input type="text"


###########################Selectores##################
**/
/**
###########################Selectores de atributos##################

$('input[type=”password”]') //todos los input de tipo password
$('input[type!=”password”]') // todos los inputs que NO son de
tipo password.
Usa !
$('a[href^=”http://”]') //todos los enlaces cuyo href comienza por “http://”. Usa ^
$('a[href$=”.com”]') //todos los enlaces cuyo href termina por “.com”. Usa $
$('img[src*=”jquery”]') //todas las imágenes cuya URL contiene la palabra jquery.
Usa *
$(“form[encoding]‟)
//todos los formularios que tengan un atributo encoding (sin
importar su valor).

###########################Selectores##################

$('ol li:last-child') // el último li de cada lista ordenada
$('ol li:nth-child(odd)') //los li impares de las listas ordenadas
$('ol li:nth-child(3n)') //los li múltiplo de 3: (3, 6, 9, 12). Es decir: uno de cada 3
$('ol li:nth-child(7n)') //los li múltiplo de 7: (7, 14, 21). Es decir: uno de cada 7
$(':header') // cualquier cabecera desde <h1> hasta <h6>


*	$("*")	All elements
#id	$("#lastname")	The element with id="lastname"
.class	$(".intro")	All elements with class="intro"
.class,.class	$(".intro,.demo")	All elements with the class "intro" or "demo"
element	$("p")	All <p> elements
el1,el2,el3	$("h1,div,p")	All <h1>, <div> and <p> elements
 	 	 
:first	$("p:first")	The first <p> element
:last	$("p:last")	The last <p> element
:even	$("tr:even")	All even <tr> elements
:odd	$("tr:odd")	All odd <tr> elements
 	 	 
:first-child	$("p:first-child")	All <p> elements that are the first child of their parent
:first-of-type	$("p:first-of-type")	All <p> elements that are the first <p> element of their parent
:last-child	$("p:last-child")	All <p> elements that are the last child of their parent
:last-of-type	$("p:last-of-type")	All <p> elements that are the last <p> element of their parent
:nth-child(n)	$("p:nth-child(2)")	All <p> elements that are the 2nd child of their parent
:nth-last-child(n)	$("p:nth-last-child(2)")	All <p> elements that are the 2nd child of their parent, counting from the last child
:nth-of-type(n)	$("p:nth-of-type(2)")	All <p> elements that are the 2nd <p> element of their parent
:nth-last-of-type(n)	$("p:nth-last-of-type(2)")	All <p> elements that are the 2nd <p> element of their parent, counting from the last child
:only-child	$("p:only-child")	All <p> elements that are the only child of their parent
:only-of-type	$("p:only-of-type")	All <p> elements that are the only child, of its type, of their parent
 	 	 
parent > child	$("div > p")	All <p> elements that are a direct child of a <div> element
parent descendant	$("div p")	All <p> elements that are descendants of a <div> element
element + next	$("div + p")	The <p> element that are next to each <div> elements
element ~ siblings	$("div ~ p")	All <p> elements that are siblings of a <div> element
 	 	 
:eq(index)	$("ul li:eq(3)")	The fourth element in a list (index starts at 0)
:gt(no)	$("ul li:gt(3)")	List elements with an index greater than 3
:lt(no)	$("ul li:lt(3)")	List elements with an index less than 3
:not(selector)	$("input:not(:empty)")	All input elements that are not empty
 	 	 
:header	$(":header")	All header elements <h1>, <h2> ...
:animated	$(":animated")	All animated elements
:focus	$(":focus")	The element that currently has focus
:contains(text)	$(":contains('Hello')")	All elements which contains the text "Hello"
:has(selector)	$("div:has(p)")	All <div> elements that have a <p> element
:empty	$(":empty")	All elements that are empty
:parent	$(":parent")	All elements that are a parent of another element
:hidden	$("p:hidden")	All hidden <p> elements
:visible	$("table:visible")	All visible tables
:root	$(":root")	The document's root element
:lang(language)	$("p:lang(de)")	All <p> elements with a lang attribute value starting with "de"
 	 	 
[attribute]	$("[href]")	All elements with a href attribute
[attribute=value]	$("[href='default.htm']")	All elements with a href attribute value equal to "default.htm"
[attribute!=value]	$("[href!='default.htm']")	All elements with a href attribute value not equal to "default.htm"
[attribute$=value]	$("[href$='.jpg']")	All elements with a href attribute value ending with ".jpg"
[attribute|=value]	$("[title|='Tomorrow']")	All elements with a title attribute value equal to 'Tomorrow', or starting with 'Tomorrow' followed by a hyphen
[attribute^=value]	$("[title^='Tom']")	All elements with a title attribute value starting with "Tom"
[attribute~=value]	$("[title~='hello']")	All elements with a title attribute value containing the specific word "hello"
[attribute*=value]	$("[title*='hello']")	All elements with a title attribute value containing the word "hello"
 	 	 
:input	$(":input")	All input elements
:text	$(":text")	All input elements with type="text"
:password	$(":password")	All input elements with type="password"
:radio	$(":radio")	All input elements with type="radio"
:checkbox	$(":checkbox")	All input elements with type="checkbox"
:submit	$(":submit")	All input elements with type="submit"
:reset	$(":reset")	All input elements with type="reset"
:button	$(":button")	All input elements with type="button"
:image	$(":image")	All input elements with type="image"
:file	$(":file")	All input elements with type="file"
:enabled	$(":enabled")	All enabled input elements
:disabled	$(":disabled")	All disabled input elements
:selected	$(":selected")	All selected input elements
:checked	$(":checked")	All checked input elements








Query	dispone	de	métodos	realmente	potentes	para	atravesar	el	DOM	de	forma	
realmente	 sencilla.	 Puedes	 encontrar	 una	 guía	 detallada	 de	 los	 mismos	 en	 la	 API	
http://api.jquery.com/category/traversing/.	Especifiquemos	aquí	los	más	utilizados.	
	
Recorriendo	el	DOM	de	forma	descendente:	
.children(selector):baja un nivel en el DOM.
.find(selector):baja varios niveles en el DOM.
.has(selector): reduce la selección actual a aquellos nodos que tengan un descendiente
que cumple con el selector.
Recorriendo el DOM de forma ascendente:
.parent(): sube un nivel en el DOM.
.parents(selector): sube varios niveles en el DOM detectando todos los ancestros que
cumplen con el selector.

.closest(selector): sube varios niveles en el DOM detectando el primer ancestro (el más
próximo) que cumple con el selector. Este método tiene en cuenta no sólo los ancestros
sino también el propio objeto.
Recorriendo el DOM horizontalmente:
.first() y .last(): filtramos una selección quedándonos sólo con el primer objeto o bien
con el último.
.next() y .next(selector): nos quedamos con el siguiente hermano. Opcionalmente
podemos indicar un selector y entonces se buscará el siguiente hermano que lo cumple.
.nextAll() y .nextAll(selector) : idem pero obtenemos todos los hermanos posteriores.
.nextUntil() y .nextUntil(selector): idem pero obtenemos todos los hermanos
posteriores hasta que se cumpla con un selector.
Para búsquedas de hermanos anteriores podemos utilizar todos los métodos equivalentes
que comienzan por prev. prev(), prevAll(), prevUntil(). Si por el contrario queremos
todos los hermanos anteriores y posteriores podemos utilizar siblings(), que también
admite opcionalmente un selector para filtrar nodos.
Métodos del DOM (combinando con selectores):
• El	trabajo	con	el	DOM	en	JQuery	es	realmente	sencillo	y	potente.	
• Habrás	observado	que	la	mayoría	de	los	métodos	incorpora	la	capacidad	de	
pasarle	como	parámetro	un	selector.	
• Recuerda:	se	admiten	selectores	CSS,	Xpath	y	Personalizados	de	JQuery.	
Ejemplo: 

$(“p”).siblings(“div:eq(0)”) // 

Por cada <p> se busca en sus hermanos
(tanto anteriores como posteriores) el primer <div>.


Podemos	insertar	al	final	del	contenido	de	un	nodo	con	append().	Si	queremos	
insertar	al	comienzo	utilizaremos	prepend().	
El	siguiente	ejemplo	agrega	un	párrafo	al	final	de	todos	los	div	con	clase	noticia:	
$('div.noticia').append('<p>Fuente: Canarias7<p>'); //	 ahora	 al	
párrafo	agregado	le	añadimos	alineación	mediante	css
$('div.noticia p').last().css('text-align', “right‟);
	
El	 siguiente	 ejemplo	 agrega	 una	 cabecera	 a	 todas	 las	 noticias	 con	 la	 clase	
urgente:	
$('div.urgente').prepend('<h1>Última hora:</h1>');
	
Los	 métodos	 append()	 y	 prepend()	 tienen	 también	 otro	 tipo	 de	
comportamiento.	 Cuando	 lo	 que	 reciben	 es	 una	 selección	 de	 objetos	 JQuery	 lo	 que	
hacen	 es	 mover	 los	 elementos	 de	 esa	 selección	 dentro	 del	 objeto	 actual.	 Observa	 el	
ejemplo.	El	elemento	con	id=”ultima”	se	moverá	al	final	del	div	con	id=”ultimaHora”:
$('div#ultimahora').append($(“#ultima‟));
	
Cuando	queremos	insertar	al	mismo	nivel	que	un	nodo	(y	no	dentro	del	mismo)	
tendremos	que	utilizar	los	métodos	before()	y	after().	En	el	siguiente	ejemplo	se	agrega	
un	nuevo	<li>	a	todas	las	listas	no	ordenadas	de	nuestra	página,	pero	queremos	hacerlo	
en	la	segunda	posición:	
	
$('ul li').first().after('<li> Éste li está en la segunda posición
de la lista </li>');
Existen	 otro	 tipo	 de	 métodos	 para	 realizar	 inserciones	 en	 el	 DOM.	 Son	
appendTo(),	prependTo(),	insertBefore()	e	insertAfter().	En	principio	son	idénticos	a	los	
ya	descritos	pero	para	utilizarlos	primero	se	pone	el	nuevo	nodo	y	como	argumento	el	
selector	del	sitio	donde	deseamos	insertar/mover:	
El	ejemplo	siguiente	agrega	el	párrafo	“Fuente:	Canarias7”	al	final	de	todos	los	
div	con	clase	noticia	(observa	cómo	el	comportamiento	es	el	contrario	al	de	append()):	
	
$('<p>Fuente: Canarias7<p>').appendTo('div.noticia');
**/


//////////////////////////Act1///////////////////////
function animar_menu() {
	$(this).find('ul').slideToggle(200	);
}
function act1() {
	$("#barra_nav").addClass("barra");
	$("#sub_barra").addClass("barranavegacion");
	$("#sub_barra > li ").addClass("primerNivel");
	$("#sub_barra  ul ").addClass("segundoNivel");
	$('#sub_barra li ul').hide();
	$('#sub_barra > li').click(animar_menu);
}
$(document).ready(act1);
/////////////////////////////////////////////////

//////////////////////////Act2///////////////////////

//////////////////////////1///////////////////////
function act2() {
	var coleccion_tr=$("#datos_tabla tr");
	for (var i = 0; i < coleccion_tr.length; i++) {
		if (i%2==0) {
			$("#datos_tabla tr")
			.eq(i).addClass('filaPar');
		}
		else{
			$("#datos_tabla tr")
			.eq(i).addClass('filaImpar');
		}
	}
	$("#datos_tabla tr")
	.eq(0).removeClass('filaPar');
	$("#datos_tabla tr")
	.eq(0).addClass('cabecera');
//////////////////////////////////////////////////	
//////////////////////////2///////////////////////	
	for (var i = 1; i < coleccion_tr.length; i++) {
		$("#datos_tabla tr").eq(i).find('td').eq(0).addClass('primeraColumna');
	}
/////////////////////////////////////////////////	
//////////////////////////3///////////////////////	
	$( "td" ).each(function() {
		var text=$(this).html().toUpperCase($(this).html());
	  	if ( text=="PRESIDENTE"){
			$(this).addClass( "presidente" );
	  	}
	});
/////////////////////////////////////////////////	
//////////////////////////4///////////////////////	
	$('#escudo').attr({
		src: 'img/objeto_dom.gif',
		alt: 'Logo del gobierno'
	});
/////////////////////////////////////////////////	


}
$(document).ready(act2);
/////////////////////////////////////////////////
//////////////////////////Act3///////////////////////

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
};
function act3() {
	//////////////////////////1//////////////////////
	$( "tr" ).each(function() {
		//var text=$(this).html().toUpperCase($(this).html());
		var letra=$(this).find('td').eq(2).text();
		letra=letra[0];
	  	if ( letra=="J"){
			$(this).find('td').first().addClass('borde_rojo');
			$(this).find('td').last().addClass('borde_rojo');
			$(this).find('td').eq(2).text($(this).find('td').eq(2).text().toUpperCase());
			$(this).find('td').eq(1).addClass('destaca');
			$(this).find('td').eq(4).addClass('destaca');
	  	}
		/////////////////////////////////////////////////	
		////////////////////////2/////////////////////////	
		var cargo=$(this).find('td').eq(3).text().toUpperCase();
		if ( cargo=="POLICÍA"){
			$(this).find('td').eq(1).addClass('policia');
			$(this).find('td').eq(4).addClass('policia');
	  	}			
		/////////////////////////////////////////////////
		////////////////////////3/////////////////////////	
		var id=$(this).find('td').eq(0).text();
		if ( id=="8"){
			$(this).find('td').eq(4).text('jaqueline@yahoo.com');
	  	}			
		/////////////////////////////////////////////////
		////////////////////////3/////////////////////////	
		var id=$(this).find('td').eq(0).text();
		if ( id=="8"){
			$(this).find('td').eq(4).text('jaqueline@yahoo.com');
	  	}			
		/////////////////////////////////////////////////	
		////////////////////////4/////////////////////////			
		var email=$(this).find('td').eq(4).text();
		if ( !(isValidEmailAddress(email))){
			$(this).find('td').eq(4).text('');	  	
		}
		/////////////////////////////////////////////////	

	});
		////////////////////////5/////////////////////////			
		$('tr:first').clone().appendTo('tfoot');
		/////////////////////////////////////////////////
		////////////////////////6/////////////////////////			
		x=0;
		var suma_votantes=0;
		var suma_blanco=0;
		$( "tr" ).each(function() {
			var responsable=$(this).find('td').eq(2).text();
			if ((x<10)&&(x!=0)){
				//var edad=prompt("Escriba Edad para  "+responsable+":");
				//$(this).find('td').eq(2).after('<td>'+edad+'</td>');
				$(this).find('td').eq(2).after('<td>'+x+'</td>');

			}
			$(this).find('th').eq(2).after('<th>Edad</th>');
			x=x+1;
		  	////////////////////////7/////////////////////////
		  	var junta=$(this).find('td').eq(1).text();
		  	if ((junta[0]=="M")||(junta[0]=="G"))
		  		$(this).find('td').eq(1).css('text-align', 'right');
		  	/////////////////////////////////////////////////
			////////////////////////8/////////////////////////
			suma_votantes+=parseInt($(this).find('td').eq(6).text()*1);
			suma_blanco+=parseInt($(this).find('td').eq(7).text()*1);
			console.log(suma_blanco+"<br>");

		});
		$(this).find('tr').last().remove(); // es lo mismo que empty, no debería
		var fila_final="<tr class='cabecera'><td colspan='6'>TOTAL:</td><td>"+suma_votantes+"</td><td>"+suma_blanco+"</td></tr>";
		$("#datos_tabla tr").last().after(fila_final); //En los apuntes aparece así

		/////////////////////////////////////////////////	
}
$(document).ready(act3);
/////////////////////////////////////////////////