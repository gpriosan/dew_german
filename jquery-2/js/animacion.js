
/*
Éstos	afectan	al	tamaño	y	a	la	opacidad	conjuntamente:	.show()	muestra,	.hide()	
oculta	y	.toggle()	alterna	entre	mostrar/ocultar.	
	
Éstos	afectan	únicamente	a	la	opacidad	(sin	alterar	el	tamaño):	
.fadeIn()	opacidad	hasta	el	100%	
.fadeOut()	opacidad	baja	al	0%.	
.fadeTo(valor)	opacidad	hasta	valor%.		
.fadeToggle()	alterna	entre	fadeIn	y	fadeOut.	
Éstos	afectan	únicamente	al	tamaño	(sin	afectar	a	la	opacidad):	
.slideDown()	 animación	 consistente	 en	 un	 deslizamiento	 desde	 arriba	 hasta	
abajo.	
.slideUp()	animación	consistente	en	un	deslizamiento	desde	abajo	hasta	arriba.	
.slideToggle()	animación	que	alterna	entre	slideDown()	y	slideUp().	
	
Todos	 los	 efectos	 anteriores	 tienen	 una	 duración	 predeterminada	 de	 400	
milisegundos.	Pero	podemos	modificar	esta	duración	en	la	mayoría	de	ellos	indicando	
como	 parámetro	 el	 número	 de	 milisegundos	 deseado	 o	 bien	 las	 palabras	 “slow”,	
“normal”	o	“fast”.	


*/
//////////////////////ACT 1 /////////////////////////////
	$(document).ready(function() {		
		
		$("#logo").fadeOut(1000,function() {
			////////////////////// 1 /////////////////////////////
			$("#logo").show();
			$("#logo").animate({"width":"25%"},1000);
			$("#logo").hide(100);
			//////////////////////fin-1 //////////////////////////
			////////////////////// 2 /////////////////////////////
			$("div[id*=err_]").hide(200);
			$("div[id*=ok_]").hide(200);
			//////////////////////fin-2///////////////////////////////
			//////////////////////3a///////////////////////////////
			$("input[type=text]").on("focusout",(function() {
				if ($(this).val()==""){
					alert("Está en blanco "+ $(this).text());
				}
				else
					alert("NO Está en blanco "+ $(this).val());
			}));
			//////////////////////3b//////////////////////////////////
			function isEmail(email) {
			  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			  return regex.test(email);
			}
			
			
			$("#cmp_email").on("focusout",(function() {
				if ($(this).val()==""){
					alert("Está en blanco "+ $(this).val());

				}
				else{
					if ( isEmail($(this).val() )){
						$('#ok_email').show(1000);	
						$('#err_email').hide(1000);	
					}
					else{
						$('#ok_email').hide(1000);
						$('#err_email').show(1000);	
					}
			
				}
			}));
			//////////////////////////////////////////////////////////
			//////////////////////3c//////////////////////////////////
			function cod(cod) {
			  var regex = /^([0-9]{5})+$/;
			  return regex.test(cod);
			}
			$("#cmp_cod_post").on("focusout",(function() {
				if ($(this).val()==""){
					alert("Está en blanco "+ $(this).val());
				}
				else{
					x=cod($(this).val());
					if ( cod($(this).val() )){
						$('#ok_cod_post').show(1000);	
						$('#err_cod_post').hide(1000);	
					}
					else{
						$('#ok_cod_post').hide(1000);
						$('#err_cod_post').show(1000);	
					}
					alert(x);
			
				}
			}));
			//////////////////////////////////////////////////////////
			//////////////////////3d//////////////////////////////////
			function telefono(telefono) {
			  var regex = /^\+?(\+34|0034|34)?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d$/;
			  return regex.test(telefono);
			}
			$("#cmd_tlf").on("focusout",(function() {
				if ($(this).val()==""){
					alert("Está en blanco "+ $(this).val());
				}
				else{
					x=telefono($(this).val());
					if ( telefono($(this).val() )){
						$('#ok_tlf').show(1000);	
						$('#err_tlf').hide(1000);	
					}
					else{
						$('#ok_tlf').hide(1000);
						$('#err_tlf').show(1000);	
					}
					alert(x);
			
				}
			}));
			//////////////////////4//////////////////////////////////
			$('#submit').click(function(event) {
				//event.preventDefault();
				if (( $('#publicidad-0').is(":checked")) || ($('#publicidad-1').is(":checked")) )
				{
					$("#err_publicidad").hide(2000);
				}
				else{
					$("#err_publicidad").show(2000);
				}
				
				if ( $("#condiciones-1").is(":checked") )
					$("#err_condiciones").show(1000);
				else
					$("#err_condiciones").hide(1000);
					
				
				
				/* Act on the event */
			});

			//////////////////////////////////////////////////////////
			
			//////////////////////fin-3///////////////////////////////

		});


	});