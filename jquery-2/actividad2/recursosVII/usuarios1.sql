-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-11-2016 a las 07:26:13
-- Versión del servidor: 5.6.29
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usuarios1`
--
CREATE DATABASE IF NOT EXISTS `usuarios1` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `usuarios1`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `andalucia`
--

CREATE TABLE `andalucia` (
  `id` int(11) NOT NULL,
  `junta_electoral` varchar(255) NOT NULL,
  `responsable` varchar(255) NOT NULL,
  `cargo` int(11) NOT NULL DEFAULT '5',
  `email` varchar(255) NOT NULL,
  `num_votantes` int(11) NOT NULL,
  `num_blanco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `andalucia`
--

TRUNCATE TABLE `andalucia`;
--
-- Volcado de datos para la tabla `andalucia`
--

INSERT INTO `andalucia` (`id`, `junta_electoral`, `responsable`, `cargo`, `email`, `num_votantes`, `num_blanco`) VALUES
(1, 'Almería', 'María Mercedes Benz Seguro', 3, 'mmbseg.es', 45257, 3507),
(2, 'Cádiz', 'Rinder Kate Proud', 3, 'rkp@gmail.es', 993327, 9194),
(3, 'Córdoba', 'Jesús Manuel Pérez Lindl', 3, 'jmperlin@hotmail.es', 653926, 6946),
(4, 'Granada', 'JJack Adrian Lang', 4, '', 74777, 5251),
(5, 'Huelva', 'JSoledad Aburrida Díaz', 2, '??', 398492, 3456),
(6, 'Jaén', 'Curly Team Papa', 3, 'papateam', 535847, 4226),
(7, 'Málaga', 'Paterquito Otoño Helado', 4, 'pater@.es', 1153047, 8424),
(8, 'Sevilla', 'Jaqueline Hernández García', 4, '', 1529822, 13713),
(9, 'Mérida', 'Brenda Trans Delte', 2, 'btrdan@cse.es', 2536, 329);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargo`
--

CREATE TABLE `cargo` (
  `id` int(255) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Truncar tablas antes de insertar `cargo`
--

TRUNCATE TABLE `cargo`;
--
-- Volcado de datos para la tabla `cargo`
--

INSERT INTO `cargo` (`id`, `descripcion`) VALUES
(1, 'Juez'),
(2, 'Policía'),
(3, 'Presidente'),
(4, 'Suplente'),
(5, 'Desconocido');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `andalucia`
--
ALTER TABLE `andalucia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Clave_cargo` (`cargo`);

--
-- Indices de la tabla `cargo`
--
ALTER TABLE `cargo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `andalucia`
--
ALTER TABLE `andalucia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `cargo`
--
ALTER TABLE `cargo`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `andalucia`
--
ALTER TABLE `andalucia`
  ADD CONSTRAINT `Clave_cargo` FOREIGN KEY (`cargo`) REFERENCES `cargo` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rangos`
--

DROP TABLE IF EXISTS `rangos`;
CREATE TABLE `rangos` (
  `id` int(11) NOT NULL,
  `minimo` int(11) NOT NULL,
  `maximo` int(11) DEFAULT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rangos`
--

INSERT INTO `rangos` (`id`, `minimo`, `maximo`, `descripcion`) VALUES
(1, 0, 500000, 'Entre 0 y 500.000'),
(2, 500000, 1000000, 'Entre 500.000 y 1.000.000'),
(3, 1000000, 1500000, 'Entre 1.000.000 y 1.500.000'),
(4, 1500000, NULL, 'Más de 1.500.000');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `rangos`
--
ALTER TABLE `rangos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `rangos`
--
ALTER TABLE `rangos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
