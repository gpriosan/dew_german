function activar(nombre,cod){
		$.ajax({
			url: 'php/obtener_Poblacion.php',
			type: 'POST',
			dataType: '',
			async:true,
			data: {munic:cod},
			})
			.done(function(resp) {
				console.log("success");
				$('#poblacion').empty();
				$('#poblacion').append("Habitantes a 1/01/12: "+resp);	
			})
			.fail(function(resp) {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
	};
$(document).ready(function() {
	function cargarMunicipiosSugerencias(){
		$.ajax({
			url: 'php/obtener_Sugerencias.php',
			type: 'POST',
			dataType: '',
			async:true,
			data: {munic:$('#municipio').val()},
			})
			.done(function(resp) {
				console.log("success");
				$('#sugerencias').empty();
				$('#sugerencias').append(resp);	
				
			})
			.fail(function(resp) {
				console.log("error");
			})
			.always(function() {
				console.log("complete");

			});
		};
	$("#municipio").keyup(cargarMunicipiosSugerencias);	
});