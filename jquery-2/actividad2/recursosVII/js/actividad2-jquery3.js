//El ready he de ponerlo bajo la función activar para que pille la misma
var id=0;
/*
	Funcion que se ejecuta cuando se hace click en alguno de los registros 
	de los que hay en los 3 selects.
	Primero vacía los campos y gracias al id enviado desde los párrafos se puede
	hacer la llamada ajax a la base pasandole dicho id.
*/	
function activar(nombre,cod){
	id=cod;
	$.ajax({
		url: 'php/obtener_votant_id.php',
		type: 'POST',
		dataType: 'json',
		async:true,
		data: {id:id},
		})
		.done(function(resp) {
			console.log("success");
			$('#updatejunta').empty();
			$('#responsable').empty();
			$('#updatecargo').empty();
			$('#updateemail').empty();
			$('#updatenumvot').empty();
			$('#updatevotblanc').empty();	
			$.each(resp,function(clave,valor) {	
				$('#updatejunta').val(valor.junta_electoral);
				$('#updateresponsable	').val(valor.responsable);
				$('#updateemail	').val(valor.email);
				$('#updatenumvot').val(valor.num_votan);
				$('#updatevotblanc').val(valor.num_blanco);

				cargo=$('#updatecargo').val();
				$.ajax({
					url: 'php/obtener_cargos.php',
					async: true,
					type: 'GET',
					dataType: '',
				})
				.done(function(resp) {
					$('#updatecargo').append(resp);						
				})
				$.getJSON({
					url: 'php/obtener_votant_cargo.php',
					async: true,
					type: 'POST',
					data: {id_cargo:valor.id},
					dataType: 'json',
				})
				.done(function(resp) {
					$('#updatecargo option[value='+valor.cargo+']').attr("checked","checked");
					$('#updatecargo option[value='+valor.cargo+']').attr("selected","true");						
				})
			});
		})
		.fail(function(resp) {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
};
$(document).ready(function() {
	function cargarVotantEmail(){
		$('#resultadosEmail').empty();
		email_n=$('#email').val();
		$.getJSON({
			url: 'php/obtener_votant_email.php',
			async: true,
			type: 'POST',
			data: {email:email_n},
			dataType: 'json',
		})
		.done(function(resp, textStatus) {
			$.each(resp,function(clave,valor) {	
				$('#resultadosEmail').append("<p onclick='activar(this,"+valor.id+")'>"+valor.responsable + "--"+ valor.email+"</p>");	
			});
		});
	};
	$("#email").change(cargarVotantEmail);
	cargarVotantEmail();
	function cargarCargos(){
		$('#cargo').empty();
		cargo=$('#cargo').val();
		$.ajax({
			url: 'php/obtener_cargos.php',
			async: true,
			type: 'GET',
			dataType: '',
		})
		.done(function(resp) {
			$('#cargo').append(resp);		
		})
		.fail(function(resp) {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
	};
	cargarCargos();
	function cargarVotantesCargos(){
		$('#resultadosCargo').empty();
		id_cargo=$("#cargo option:checked").val()
		if (id_cargo==undefined){
			id_cargo="1";
		}
		else{
			id_cargo=$(this).val()
		}
		$.getJSON({
			url: 'php/obtener_votant_cargo.php',
			async: true,
			type: 'POST',
			data: {id_cargo:id_cargo},
			dataType: 'json',
		})
		.done(function(resp, textStatus) {
			$('#resultadosCargo').append("Hay "+resp.length+ " cargos.");	
			$.each(resp,function(clave,valor) {	
				$('#resultadosCargo').append("<p onclick='activar(this,"+valor.id+")'>"+valor.responsable + "--"+ valor.email+"</p>");	
			})
		})
		.fail(function(resp) {
			console.log("error::");
			console.log(resp);
		})
		.always(function() {	
			console.log("complete::");
		});
	};
	$("#cargo").change(cargarVotantesCargos);
	$("#cargo").trigger('change')
	function cargarVotantRango(){
		$('#resultadosVotantes').empty();
		minimo=0;
		maximo=10000;
		if ($("#votantes").val()==1){
			minimo=0;
			maximo=500000;
		}
		if ($("#votantes").val()==2){
			minimo=500000;
			maximo=1000000;
		}
		if ($("#votantes").val()==3){
			minimo=1000000;
			maximo=1500000;
		}
		if ($("#votantes").val()==4){
			minimo=15000000;
			maximo=10000500000;
		}
		$.getJSON({
			url: 'php/obtener_votant_num_vot.php',
			async: true,
			type: 'POST',
			data: {min:minimo,max:maximo},
			dataType: 'json',
		})
		.done(function(resp, textStatus) {
			$('#resultadosVotantes').append("Hay "+resp.length+ " registros.");
			$.each(resp,function(clave,valor) {	
				$('#resultadosVotantes').append("<p onclick='activar(this,"+valor.id+")'>"+valor.responsable + "--"+ valor.email+"</p>");	
			});
		});
	};
	$("#votantes").change(cargarVotantRango);
	cargarVotantRango();
	function cargarRangos(){
		$('#votantes').empty();
		email_n=$('#minimo').val();
		$.getJSON({
			url: 'php/obtener_rangos.php',
			async: true,
			type: 'POST',
			dataType: '',
		})
		.done(function(resp) {
			$('#votantes').append(resp);			
		})
	};
	cargarRangos();

	function grabar(){
		var identificador = id;
		var junta = $('#updatejunta').val();
		var responsable=$('#updateresponsable	').val();
		var cargo=$('#updatecargo').val();
		var email=$('#updateemail	').val();
		var votan =$('#updatenumvot').val();
		var blanco =$('#updatevotblanc').val();

		if ( (identificador==0)||(junta=="")||(responsable=="")||(cargo=="")||(email=="")||(votan=="")||(blanco=="") ){
			alert("Faltan datos por rellenar para actualizar el registro");
		}
		else{
			$.ajax({
			url: 'php/updateRegistro.php',
			type: 'POST',
			dataType: 'json',
			async:true,
			data: {id:id,junta_electoral:junta,responsable:responsable,cargo:cargo,email:email,num_votan:votan,num_blanco:blanco},
			})
			.done(function(resp) {
				console.log("success");	
				$("#updateDone").append('Los datos se han modificado bien.')

			})
			.fail(function(resp) {
				console.log("error");
				console.log(resp);	
			})
			.always(function() {
				console.log("complete");
			});
		}
	}
	$("#update").click(grabar);
});