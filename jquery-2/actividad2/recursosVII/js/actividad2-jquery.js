$(document).ready(function() {
	$.ajax({
		url: 'php/obtener_islas.php',
		type: 'GET',
		dataType: '',
		data: $('islas').serialize(),
	})
	.done(function(resp) {
		console.log("success");
		$('#islas').append(resp);
		cargarMunicipios();//Hacemos aquí la llamada ya que si la hacemos despúes del change(cargarmunicipios) no cargará los municipios
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	function cargarMunicipios(){
		$("#obtener").attr('disabled','disabled');
		$("#municipiosLoading").append('<img src="imagenes/ajax_clock_small.gif"> Cargando')
		$.ajax({
			url: 'php/obtener_municipios.php',
			type: 'GET',
			dataType: '',
			data: {isla:$('#islas').val()},
			})
			.done(function(resp) {
				console.log("success");
				$('#municipios').empty();
				$('#municipios').append(resp);	
				$("#municipiosLoading").empty();
				$("#obtener").removeAttr('disabled','disabled');	
			})
			.fail(function(resp) {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		};
	$("#islas").change(cargarMunicipios);	
	function filtrarVotantes(){
		$.ajax({
			url: 'php/obtener_votantes2.php',
			type: 'POST',
			dataType: '',
			data: {responsable:$('#responsableText').val(),votantes:$('#votanteText').val()},
			})
			.done(function(resp) {
				console.log("success");
				$('#resultadosVotantes').empty();
				$('#resultadosVotantes').append(resp);	
				var filas=$('tr');
				filas.eq(0).append('<th>Editar</th><th>Eliminar</th>');
				filas.each(function(i) {
					if (i!=0)
					{
						filas.eq(i).append('<td><img src="imagenes/editar.png"></td>');
						filas.eq(i).append('<td><img src="imagenes/eliminar.png"></td>');
					}
				});
			})
			.fail(function(resp) {
				console.log("error");
			})
			.always(function() {
				console.log("complete");
			});
		};
	$("#obtener").click(filtrarVotantes);
});