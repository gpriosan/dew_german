var objetoAjax;
var objetoAjax2;
///////////////////////INICIO ACT 4 //////////////7

function agregaEvento(evento, objeto, funcion) {
	if (objeto.addEventListener) // estamos en un DOM del estándar W3C
		objeto.addEventListener(evento,funcion,false);
	else if (objeto.attachEvent) // estamos en un DOM de IExplorer
		objeto.attachEvent("on"+evento, funcion);
	else // no estamos en un DOM ni W3C ni de IExplorer ? modelo tradicional
	objeto["on"+evento] = funcion; // recuerda: objetos son arrays asociativos
}
agregaEvento("load", window, iniciar);

function crearAjax() {
	var objeto = null; // será el objeto a crear
	try {
		objeto = new XMLHttpRequest(); // navegadores estándar
	} catch (error1) {
		try {
			objeto = new ActiveXObject("Microsoft.XMLHTTP"); // IE con librería MSXML 2.0 o superior
		} catch (error2) {
			try {
				objeto = new ActiveXObject("Msxml12.XMLHTTP"); // IE con librería MSXML 1.2
			} catch (error3) {
				objeto = false; // no hemos logrado crear el objeto, retornamos false
			}
		}
	}
	return objeto; // devolvemos el objeto
}
function limpiar(){
	var poblacion = document.getElementById("poblacion").innerHTML="";
	var sugerencias = document.getElementById("sugerencias").innerHTML="";
	var municipio = document.getElementById("municipio").value="";
}
function iniciar () {
	var municipio = document.getElementById("municipio").value;
	var muni = document.getElementById("municipio");
	
	objetoAjax = crearAjax();
	if (!objetoAjax) {
		alert("No se ha podido crear el objeto ajax relacionado con las islas");
		return;
	}
	else{
		agregaEvento("keyup", muni, llamadaSoltarTecla);
		agregaEvento("focus", muni, limpiar);
	}
	objetoAjax.onreadystatechange = sugerencias;
	objetoAjax.open("POST", "php/obtener_Sugerencias.php", true);
	try{
		objetoAjax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="munic="+encodeURIComponent(municipio);
		objetoAjax.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function llamadaSoltarTecla(){
	var municipio = document.getElementById("municipio").value;
	objetoAjax.open("POST", "php/obtener_Sugerencias.php", true);
	try{
		objetoAjax.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="munic="+encodeURIComponent(municipio);
		objetoAjax.send(datos);
		objetoAjax.onreadystatechange = sugerencias;
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function sugerencias() {
	var sugerencias = document.getElementById("sugerencias");
	if (this.readyState == 0) { // No hay peticiones
		objetoAjax.onreadystatechange = sugerencias;
	}
	if (this.readyState == 4) { // la petición terminó
		objetoAjax.onreadystatechange = sugerencias;
		switch (this.status) {
			case 200: // la petición terminó con éxito
				sugerencias.innerHTML = objetoAjax.responseText;
				sugerencias.innerHTML += "Estado 200: "+ this.statusText;
				break;
			case 404: // no se encontró el recurso
				sugerencias.innerHTML = "No es posible recuperar las sugerencias <br> Error 404 :"+this.statusText;
				break;
			default:
				sugerencias.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
function activar(nombre,cod){
	municipio.value=nombre.innerHTML;
	objetoAjax2 = crearAjax();
	if (!objetoAjax2) {
		alert("No se ha podido crear el objeto ajax relacionado con el cod");
		return;
	}
	objetoAjax2.onreadystatechange = obtenerPoblacion;
	objetoAjax2.open("POST", "php/obtener_Poblacion.php", true);
	try{
		objetoAjax2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		var datos="munic="+encodeURIComponent(cod);
		objetoAjax2.send(datos);
	} catch (e){
		alert("Error al establecer la cabecera"+e.description);
	}
}
function obtenerPoblacion() {
	var poblacion = document.getElementById("poblacion");
	var sugerencias = document.getElementById("sugerencias");
	if (this.readyState == 4) { // la petición terminó
		switch (this.status) {
			case 200: // la petición terminó con éxito
				poblacion.innerHTML = objetoAjax2.responseText;
				poblacion.innerHTML += " habitantes a 1 de enero de 2012 <br>";
				poblacion.innerHTML += "Estado 200: "+ this.statusText;
				sugerencias.innerHTML="";
				break;
			case 404: // no se encontró el recurso
				poblacion.innerHTML = "No es posible recuperar las sugerencias <br> Error 404 :"+this.statusText;
				break;
			default:
				poblacion.innerHTML = "Error " + this.status + ": ";
			break;
		}
	}
}
///////////////////////FIN ACT 4 //////////////7