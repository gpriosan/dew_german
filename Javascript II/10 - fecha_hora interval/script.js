function validaOp(op){
	var patron = /^([\d]*[\s]*[\d]*)*$/;
	//var patron = /^[A-Z]{1,2}\s\d{4}\s([B-D]|[F-H]|[J-N]|[P-T]|[V-Z]){3}$/;
  if(patron.test(op))
	return true
  else 
    return false
}
function fecha_hora(){
	var f=""
	var r=document.getElementById("resultado");
	var v=validaOp("12 12")
	var fecha=new Date();
	var fecha_final="";
	
	f=fecha.toLocaleDateString();
	diaLetra=fecha.getDay();
	mes=fecha.getMonth();
	año=fecha.getYear()+1900;
	hora=fecha.toLocaleTimeString();
	fecha_final+=hora+" ";

	switch(diaLetra){
		case 0:
			fecha_final+="Domingo";
			break;
		case 1:
			fecha_final+="Lunes";
			break;	
		case 2:
			fecha_final+="Martes";
			break;
		case 3:
			fecha_final+="Miércoles";
			break;
		case 4:
			fecha_final+="Jueves";
			break;
		case 5:
			fecha_final+="Viernes";
			break;
		case 6:
			fecha_final+="Sábado";
			break;					
	}
	fecha_final+=" "+fecha.getDate()+" ";

	switch(mes){
		case 0:
			fecha_final+="Enero";
			break;
		case 1:
			fecha_final+="Febrero";
			break;	
		case 2:
			fecha_final+="Marzo";
			break;
		case 3:
			fecha_final+="Abril";
			break;
		case 4:
			fecha_final+="Mayo";
			break;
		case 5:
			fecha_final+="Junio";
			break;
		case 6:
			fecha_final+="Julio";
			break;					
		case 7:
			fecha_final+="Agosto";
			break;					
		case 8:
			fecha_final+="Septiembre";
			break;					
		case 9:
			fecha_final+="Octubre";	
			break;				
		case 10:
			fecha_final+="Noviembre";
			break;					
		case 11:
			fecha_final+="Diciembre";
			break;
	}
	fecha_final+=" de "+año;
	if ( fecha==""){
		x='No existe fecha';
		r.innerHTML=x;
	}
	else if ( (v==false)){
		x='Indique una operación';
		r.innerHTML=x;
		throw(new Error('Indique operación válida (EJ:12 14 12)'));
	}
	else{
		r.innerHTML=fecha_final;
	}
}
setInterval(fecha_hora,1000);
//clearInterval(x);