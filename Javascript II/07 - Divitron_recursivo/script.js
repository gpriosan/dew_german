function validaOp(op){
	var patron = /^([\d]*[\s]*[\d]*)*$/;
	//var patron = /^[A-Z]{1,2}\s\d{4}\s([B-D]|[F-H]|[J-N]|[P-T]|[V-Z]){3}$/;
  if(patron.test(op))
	return true
  else 
    return false
}
var arrayE=[];
var valor=0;
function dividir(n){
	if (n==arrayE.length-1){
		return arrayE[0];
	}
	else{
		arrayE[n]=arrayE[0]/arrayE[n+1]
		arrayE[0]=arrayE[n];
		console.log("--->"+arrayE[0]);
		return dividir(n+1);
	}	
}
function dividirRecursivo(){
	if (arrayE.length==1)
		return arrayE[0];
	var divisor=arrayE.pop();
	console.log("Divisor: "+divisor+" Array: "+arrayE.toString());
	return dividirRecursivo()/divisor;
	/*
		100 2 2 5
		1º Apila: dividirRecursivo()/5 -> dividirRecursivo()/2 -> dividirRecursivo()/2
		2º return arrayE[0] devuelve el primer valor del array (100)
		3º Calcula: 100/5 <- 50/2 <- 25/2
	*/
}
function comprobar(){
	var text=document.getElementsByName("mensaje");
	r=document.getElementById("resultado");	
	var m = text[0].value;
	m=m.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, ''); // Se eliminan espacios dobles y espacios y tabulaciones del principio y del final
	v=validaOp(m)
	arrayE=m.split(" ");
	
	if ( (text[0].value=="")){
		x='Indique una operación (EJ:12 14 12)';
		r.innerHTML=x;
	}
	else if ( (v==false)){
		x='Indique una operación ---';
		r.innerHTML=x;
		throw(new Error('Indique operación válida (EJ:12 14 12)'));
	}
	else{
		var valor=""+dividirRecursivo();
		console.log(valor);
		var arr=valor.split(".");
		valor =parseFloat(valor);
		if (arr[1]>0){
			r.innerHTML=valor.toFixed(2);
		}
		else
			r.innerHTML=valor;
	}
}