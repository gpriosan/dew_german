function validaOp(n){
	var patron = /^([\d])*$/;
	if(patron.test(n))
		return true
	else 
		return false
}

var matriz=new Array();
var prohibidas=new Array();
var recorrido= new Array();
var desrecorrido= new Array();
var tablero=document.getElementById("tablero");
var boton=document.getElementById("boton1");
var boton2=document.getElementById("boton2");
var victoria=document.getElementById("victoria");
var laberinto="";
var posicion="pos00";
var siguiente;
var actual;
var cancela_intervalo=0;
var fin;
var intervalo;
var ninter;
var comprueba_pos_en_array;
var comprueba_pos_en_desrecorrido;
var seharecorrido=0;
var IndiceI;
var indiceJ;
var sindireccion=1;
function construirLaberinto(n){
	for (var i = 0; i < n; i++) {
		matriz[i]=new Array();
		for (var j = 0; j < n; j++) {
			matriz[i][j]="";
		}	
	}
	laberinto+="<table>";
	for (var i = 0; i < n; i++) {
		laberinto+="<tr>";
		for (var j = 0; j < n; j++) {
			aleatorio=parseInt(Math.random() * (4 - 1) + 1);
			if ( (i==0) && (j==0)){
				laberinto+="<td class='aquiando' id='pos"+i+""+j+"'>";
			}
			else if ( (i==n-1) && (j==n-1)){
				laberinto+="<td id='pos"+i+""+j+"'>";
			}
			else if ( (aleatorio==1)  ) {
				laberinto+="<td class='prohibida' id='pos"+i+""+j+"'>";
				prohibidas.push("pos"+i+""+j);
			}
			else
				laberinto+="<td id='pos"+i+""+j+"'>";
			matriz[i][j]=("pos"+i+""+j+"");
			laberinto+="pos"+i+""+j+"";	
			laberinto+="</td>";
		}
		laberinto+="</tr>";
	}
	laberinto+="</table>";
	boton.style="display:none;";
	boton2.style="display:block;";	
	fin="pos"+(matriz[0].length-1).toString()+""+(matriz[0].length-1).toString();
}
function retrocede(){
	console.log("Recorrido : "+recorrido.toString());
	console.log("Desrecorrido: "+desrecorrido.toString());
	anterior=desrecorrido.pop();
	//recorrido.push(anterior);
	avanza(anterior)
}
function estaProhibida(siguiente){
	if (siguiente.className=="prohibida")
		return 0;
	else
		return 1;
}
function vaciaRecorrido(){
	desrecorrido=recorrido;
	recorrido=[];
	seharecorrido=1;
}
function mvDerecha(){
	indiceJ+=1;
	siguiente=document.getElementById("pos"+indiceI+""+indiceJ);
	comprueba_pos_en_array=recorrido.indexOf(siguiente.id);
	comprueba_pos_en_desrecorrido=desrecorrido.indexOf(siguiente.id);
	if  ( (document.getElementById("pos"+indiceI+""+indiceJ)!=null) && (estaProhibida(siguiente)==1) ) {
		if ( (comprueba_pos_en_array===-1) && (comprueba_pos_en_desrecorrido===-1) ){
			actual.style="background-color:black !important";sindireccion=1;
			avanza(siguiente.id);
		}
		else{
			console.log("Derecha: Esa posición ya la has pasado, busca una mejor alternativa Posicion: "+siguiente.id);
			indiceJ-=1;sindireccion+=1;
		}
	}
	else{
		indiceJ-=1;sindireccion+=1;
	}
	
}
function mvAbajo(){
	indiceI+=1;
	siguiente=document.getElementById("pos"+indiceI+""+indiceJ);
	comprueba_pos_en_array=recorrido.indexOf(siguiente.id);
	comprueba_pos_en_desrecorrido=desrecorrido.indexOf(siguiente.id);
	if  ( (document.getElementById("pos"+indiceI+""+indiceJ)!=null) && (estaProhibida(siguiente)==1) ) {
		if ( (comprueba_pos_en_array===-1) && (comprueba_pos_en_desrecorrido===-1) ){
			actual.style="background-color:black !important";sindireccion=2;
			avanza(siguiente.id);
		}
		else{
			console.log("Derecha: Esa posición ya la has pasado, busca una mejor alternativa Posicion: "+siguiente.id);
			indiceI-=1;sindireccion+=1;
		}
	}
	else{
		indiceI-=1;sindireccion+=1;
	}
	
}	
function mvIzquierda(){
	indiceJ-=1;
	siguiente=document.getElementById("pos"+indiceI+""+indiceJ);
	comprueba_pos_en_array=recorrido.indexOf(siguiente.id);
	comprueba_pos_en_desrecorrido=desrecorrido.indexOf(siguiente.id);
	if  ( (document.getElementById("pos"+indiceI+""+indiceJ)!=null) && (estaProhibida(siguiente)==1) ) {
		if ( (comprueba_pos_en_array===-1) && (comprueba_pos_en_desrecorrido===-1) ){
			actual.style="background-color:black !important";sindireccion=3;
			avanza(siguiente.id);
		}
		else{
			console.log("Derecha: Esa posición ya la has pasado, busca una mejor alternativa Posicion: "+siguiente.id);
			indiceJ+=1;sindireccion+=1;
		}
	}
	else{
		indiceJ+=1;sindireccion+=1;
	}
	
}
function mvArriba(){
	indiceI+=1;
	siguiente=document.getElementById("pos"+indiceI+""+indiceJ);
	comprueba_pos_en_array=recorrido.indexOf(siguiente.id);
	comprueba_pos_en_desrecorrido=desrecorrido.indexOf(siguiente.id);
	if  ( (document.getElementById("pos"+indiceI+""+indiceJ)!=null) && (estaProhibida(siguiente)==1) ) {
		if ( (comprueba_pos_en_array===-1) && (comprueba_pos_en_desrecorrido===-1) ){
			actual.style="background-color:black !important";sindireccion=4;
			avanza(siguiente.id);
		}
		else{
			console.log("Derecha: Esa posición ya la has pasado, busca una mejor alternativa Posicion: "+siguiente.id);
			indiceI-=1;sindireccion+=1;
		}
	}
	else{
		indiceI-=1;sindireccion+=1;
	}
	
}
function avanza(pos){
	console.log("Estoy en la casilla: "+pos);
	siguiente.style="background-color:green !important;";
	if (pos!="pos00")
		recorrido.push(pos);
	console.log("Se envia posición: "+pos);
	posicion=pos;
	if (siguiente.id==fin){
		victoria.innerHTML=("Has salido!!! ");
		clearInterval(intervalo);
		console.log("Recorrido realizado: "+recorrido.toString());
		intervalo=0;
		//ninter=setInterval(salirDeAqui,2500);
	}
	else{
		if(sindireccion==1){
			mvDerecha();
		}
		else if (sindireccion==2){
			mvAbajo();	
		}
		else if (sindireccion==3){
			mvIzquierda();
		}
		else if (sindireccion==4){
			mvArriba();
		}
		else{
			sindireccion=5;
			clearInterval(intervalo);
			return 0;
		}
	}
}

function salirDeAqui(){
	clearInterval(ninter);
	actual=document.getElementById(posicion);
	indiceI=parseInt(posicion[posicion.length-2]);
	indiceJ=parseInt(posicion[posicion.length-1]);
	console.log("IndiceI: "+indiceI+ " IndiceJ: "+ indiceJ)
	siguiente=document.getElementById("pos"+indiceI+""+indiceJ);
	console.log("------------------");
	console.log(comprueba_pos_en_array);
	console.log("------------------");
	console.log("Recorrido : "+recorrido.toString());
	console.log("------------------");
	console.log("------------------");
	console.log("Desrecorrido: "+desrecorrido.toString());
	console.log("------------------");
	comprueba_pos_en_array=0;
	comprueba_pos_en_desrecorrido=0;
	if((sindireccion!=0) || (sindireccion!=5)){
		avanza(siguiente.id);
	}
	comprueba_pos_en_array=recorrido.includes("pos00");
	if ( (comprueba_pos_en_array==true) || (sindireccion==5)){
		victoria.innerHTML=("No tienes escapatoria... ");
		clearInterval(intervalo);
		clearInterval(ninter);
		return 0;
	}
	clearInterval(intervalo);

}
function comprobar(){
	var numero=document.getElementById("mensaje").value;
	numero=numero.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, ''); 
	// Se eliminan espacios dobles y espacios y tabulaciones del principio y del final
	numero=parseInt(numero);
	v=validaOp(numero);
	if ( (numero=="")){
		x='Indique un número valido  (EJ:14)';
		tablero.innerHTML=x;
	}
	else if ( (v==false)){
		x='Indique un número valido (EJ:12)';
		tablero.innerHTML=x;
		throw(new Error('Indique un número valido (EJ:12)'));
	}
	else{
		construirLaberinto(numero);
		tablero.innerHTML=laberinto;

		intervalo=setInterval(salirDeAqui,2500);
		if (intervalo==0)
			clearInterval(intervalo);
	}
}