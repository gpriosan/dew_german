function validaOp(n){
	var patron = /^([\d])*$/;
  	if(patron.test(n))
		return true
  	else 
    	return false
}
var laberinto="";
var matriz=new Array();
var prohibidas=new Array();
var tablero=document.getElementById("tablero");
var boton=document.getElementById("boton1");
var boton2=document.getElementById("boton2");
var victoria=document.getElementById("victoria");
var posicion="pos00";
var siguiente;
var cancela_intervalo=0;
function construirLaberinto(n){
	for (var i = 0; i < n; i++) {
		matriz[i]=new Array();
		for (var j = 0; j < n; j++) {
			matriz[i][j]="";
		}	
	}
	laberinto+="<table>";
	for (var i = 0; i < n; i++) {
		laberinto+="<tr>";
		for (var j = 0; j < n; j++) {
			aleatorio=parseInt(Math.random() * (5 - 1) + 1);
			if ( (i==0) && (j==0)){
				laberinto+="<td class='aquiando' id='pos"+i+""+j+"'>";
			}
			else if ( (aleatorio==1) && ( ( (j!=0) && (i!=0) ) && ( (j!=n-1) && (i!=n-1) ) ) ){
				laberinto+="<td class='prohibida' id='pos"+i+""+j+"'>";
				prohibidas.push("pos"+i+""+j);
			}
			else
				laberinto+="<td id='pos"+i+""+j+"'>";
			matriz[i][j]=("pos"+i+""+j+"");
			laberinto+="pos"+i+""+j+"";	
			laberinto+="</td>";
		}
		laberinto+="</tr>";
	}
	laberinto+="</table>";
	boton.style="display:none;";
	boton2.style="display:block;";	
}
function salirDeAqui(posicion){
	var actual=document.getElementById(posicion);
	//Fin de partida//
	var fin="pos"+(matriz[0].length-1).toString()+""+(matriz[0].length-1).toString();
	//Fin de partida//
	var indiceI=parseInt(posicion[posicion.length-1]);
	var indiceJ=parseInt(posicion[posicion.length-2]);
	if (posicion==fin){
		victoria.innerHTML=("Has salido!!!");
		cancela_intervalo=1;
		return 0;
	}
	else{
		for (var i = indiceI-1; i <= indiceI+1; i++) {
			for (var j = indiceJ-1; j <= indiceJ+1; j++) {
				if ( (document.getElementById("pos"+i+""+j))==null){
					console.log("No existe celda en pos: pos"+i+""+j);
				}
				else{
					siguiente=document.getElementById("pos"+i+""+j);
					console.log("Posicion Actual: "+posicion+" -------------------(siguiente)");
					if (siguiente.className=="prohibida"){
						console.log("La celda en posición: pos"+i+""+j+" está prohibida");
					}
					else{
						actual=document.getElementById(posicion);
						actual.style="background-color:black !important";
						siguiente.style="background-color:green;"
						anterior=posicion;
						posicion=siguiente.id;					
					}
				}
			}		
		}
	}
	var intervalo=setInterval(function(){
		if (cancela_intervalo==1){
			clearInterval(intervalo);			
			console.log("---------------------");
		}
		else
			salirDeAqui(posicion);},2500);

}

function comprobar(){
	var numero=document.getElementById("mensaje").value;
	//console.log(numero);
	numero=numero.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, ''); 
	// Se eliminan espacios dobles y espacios y tabulaciones del principio y del final
	numero=parseInt(numero);
	v=validaOp(numero);
	if ( (numero=="")){
		x='Indique un número valido  (EJ:14)';
		tablero.innerHTML=x;
	}
	else if ( (v==false)){
		x='Indique un número valido (EJ:12)';
		tablero.innerHTML=x;
		throw(new Error('Indique un número valido (EJ:12)'));
	}
	else{
		construirLaberinto(numero);
		tablero.innerHTML=laberinto;
	}
}