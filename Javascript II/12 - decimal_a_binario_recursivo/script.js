function validaOp(op){
	var patron = /^([\d])*$/;
	//var patron = /^[A-Z]{1,2}\s\d{4}\s([B-D]|[F-H]|[J-N]|[P-T]|[V-Z]){3}$/;
  if(patron.test(op))
	return true
  else 
    return false
}
var binario=['0','1'];
var base=2
function binarioRecursivo(n){
	if (n<base)
		return n;
	return ""+binarioRecursivo(parseInt(n/base))+""+binario[n%base];
}
function comprobar(){
	var text=document.getElementsByName("mensaje");
	r=document.getElementById("resultado");	
	var numero = text[0].value;
	numero=numero.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, ''); // Se eliminan espacios dobles y espacios y tabulaciones del principio y del final
	v=validaOp(numero);
	if ( (text[0].value=="")){
		x='Indique un número valido  (EJ:14)';
		r.innerHTML=x;
	}
	else if ( (v==false)){
		x='Indique un número valido (EJ:12)';
		r.innerHTML=x;
		throw(new Error('Indique un número valido (EJ:12)'));
	}
	else{
		binario2=binarioRecursivo(numero);
		r.innerHTML=binario2;
	}
}